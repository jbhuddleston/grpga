# grpga

Foundry Virtual TableTop support for a Generic RolePlaying Game Aid (GRPGA).

The primary goal of GRPGA is to permit users to play their game, in their way, online at least as easily as if they were sitting around a table; any further automation, character generation or advancement tracking features have been added for convenience where they make sense.

GRPGA currently supports play of:
- GURPS (3D6)
- Palladium games (D120) - with combat tracker
- Hijacks of the Palladium ruleset swap the Skills, Defences and Attacks D20 rolls for other systems dice:
    - D6Pool - Shadowrun 4e
    - D20 >= Target Number (TN)
    - 1D10, 2D10 >= TN
    - 2D6, 3D6 >= TN
    - Alternity, Sine Nomine, D616, Novus2e
    - D100 <= TN (Percent)
- Rolemaster family of games (D100)
- Mutants and Masterminds 3e
- Bare Bones Fantasy (Percent)
- Mini-Six (D6Pips)

Development of a specific ruleset depends on the active participation and feedback of players and GMs. If you want to see something for your ruleset, then ask for it and show me how it needs to work.

A long-term goal is to capture the advantages of each system and create my own based on timeline tactical activity instead of turn-based action.

To join the discussion or monitor progress on Discord, join here; https://discord.gg/d8ujbNG Please let me know on arrival which game system or homebrew effort led you to investigate my project. Feel free to ask for guidance or assistance. Thank you for your interest.

I employ my YouTube channel https://www.youtube.com/channel/UC-WeR6LCq_UuPpldldbGDvw to provide guidance on how to use this system and others I am developing that are based on it. Reviewing those other system videos may provide insights as to how all my systems behave and thus be useful to you.

If you find this useful and feel that you are able to support my further work on this project, please do buy me a coffee, https://ko-fi.com/jbhuddleston, I thank you.
