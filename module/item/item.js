/**
 * Extend the basic Item with some very simple modifications.
 * @extends {Item}
 */
export class BaseItem extends Item {
  /**
   * Augment the basic Item data model with additional dynamic data.
   */
  prepareDerivedData() {
    const itemdata = this.system;
    switch (this.type) {
      case "Variable": {
        if (!Array.isArray(itemdata.entries)) {
          itemdata.entries = Object.values(itemdata.entries);
        }
        const entries = itemdata.entries;
        let notfound = true;
        for (const entry of entries) {
          if (entry.label != itemdata.label && itemdata.label != "") continue;
          notfound = false;
          if (Number.isNumeric(entry.formula)) {
            entry.value = Number(entry.formula);
          } else {
            entry.value = 0;
          }
          itemdata.moddedformula = itemdata.formula = entry.formula;
          itemdata.value = itemdata.moddedvalue = entry.value;
        }
        if (notfound && entries.length > 0) {
          itemdata.formula = entries[0].formula;
          itemdata.label = entries[0].label;
        }
        break;
      }
      case "Modifier": {
        if (itemdata.alwaysOn) itemdata.inEffect = true;
        itemdata.attack = false;
        itemdata.damage = false;
        itemdata.defence = false;
        itemdata.reaction = false;
        itemdata.skill = false;
        itemdata.spell = false;
        itemdata.check = false;
        itemdata.pool = false;
        itemdata.primary = false;
        if (!Array.isArray(itemdata.entries)) {
          itemdata.entries = Object.values(itemdata.entries);
        }
        const entries = itemdata.entries;
        for (let i = 0; i < entries.length; i++) {
          switch (entries[i].category) {
            case "attack": itemdata.attack = true; break;
            case "damage": itemdata.damage = true; break;
            case "defence": itemdata.defence = true; break;
            case "reaction": itemdata.reaction = true; break;
            case "skill": itemdata.skill = true; break;
            case "spell": itemdata.spell = true; break;
            case "check": itemdata.check = true; break;
            case "pool": itemdata.pool = true; break;
            case "primary": itemdata.primary = true; break;
          }
          if (Number.isNumeric(entries[i].formula)) {
            // the formula is a number
            entries[i].value = Number(entries[i].formula);
          } else if (entries[i].formula.includes("#") || entries[i].formula.includes("@")) {
            // the formula has ranks or a reference and will be processed in prepareDerivedData
          } else {
            // the formula should be a valid dice expression so has no value
            entries[i].value = 0;
            entries[i].moddedvalue = 0;
            entries[i].moddedformula = entries[i].formula;
          }
        }
        break;
      }
      case "Melee-Attack":
      case "Ranged-Attack":
      case "Advantage":
      case "Power":
      case "Defence":
      case "Rollable": {
        if (Number.isNumeric(itemdata.formula)) {
          // the formula is a number
          itemdata.value = Number(itemdata.formula);
        } else if (itemdata.formula.includes("#") || itemdata.formula.includes("@")) {
          // the formula has ranks or a reference and will be processed in prepareDerivedData
        } else {
          // the formula should be a valid dice expression so has no value
          itemdata.value = 0;
          itemdata.moddedvalue = 0;
          itemdata.moddedformula = itemdata.formula;
        }
        break;
      }
      case "Container": {
        if (itemdata.notes != "") {
          try {
            const items = JSON.parse(itemdata.notes.replace(/\\\\?n/g,"\\n"));
            // if the entries are not an array, replace them with one
            if (!Array.isArray(itemdata.entries))
              itemdata.entries = [];
            if (items.length > 0) {
              for (let item of items) {
                itemdata.entries.push(item);
              }
              itemdata.notes = "";
              // if the dropped items does not exist or is not an array, add them
              if (!Array.isArray(itemdata.dropped))
                itemdata.dropped = [];
              if (this.actor) {
                const actoritems = Object.values(this.actor.items)[1];
                for (let item of actoritems) {
                  const elm = itemdata.entries.find((i) => {
                    return i.name == item.name && i.type == item.type
                  });
                  if (elm) itemdata.dropped.push(item._id);
                }
              } else {
                itemdata.dropped = [];
              }
              this.update({ system: itemdata });
            }
          } catch (err) {
            ui.notifications.error("Something is not right in the notes of this Container.");
          }
        }
        break;
      }
      case "Pool": {
        if (Number.isNumeric(itemdata.maxForm)) {
          // the formula is a number
          itemdata.max = Number(itemdata.maxForm);
          itemdata.hasmax = true;
        } else if (itemdata.maxForm.includes("@") || itemdata.maxForm.includes("#")) {
          // the formula has a reference and will be processed in prepareDerivedData
          itemdata.hasmax = false;
        } else {
          // the formula should be a valid dice expression so has no value
          itemdata.max = 0;
          itemdata.hasmax = true;
        }
        if (Number.isNumeric(itemdata.minForm)) {
          // the formula is a number
          itemdata.min = Number(itemdata.minForm);
          itemdata.hasmin = true;
        } else if (itemdata.minForm.includes("@") || itemdata.minForm.includes("#")) {
          // the formula has a reference and will be processed in prepareDerivedData
          itemdata.hasmin = false;
        } else {
          // the formula should be a valid dice expression so has no value
          itemdata.min = 0;
          itemdata.hasmin = true;
        }
        if (itemdata.hasmax && itemdata.value > itemdata.max) itemdata.value = itemdata.max;
        if (itemdata.hasmin && itemdata.value < itemdata.min) itemdata.value = itemdata.min;
        break;
      }
      default: {
        // do nothing yet
      }
    }
  }
  /**
   * Set the label of a variable so it will calculate the desired value.
   */
  async setValue(targetlabel) {
    const itemdata = this.system;
    switch (this.type) {
      case "Variable": {
        const entries = itemdata.entries
        for (let i = 0; i < entries.length; i++) {
          if (targetlabel == entries[i].label.split(":")[0]) {
            await this.actor.updateEmbeddedDocuments("Item", [{ _id: this._id, system: { label: entries[i].label } }]);
            break;
          }
        }
        break;
      }
      default: {
        // do nothing yet
      }
    }
  }

  /** @override */
  static async createDialog(data={}, {parent=null, pack=null, ...options}={}) {

    // Collect data
    const documentName = this.metadata.name;

    // HERE WE PRESENT AN ABBREVIATED LIST BASED ON RULESET
    const types = CONFIG.system.itemtypes; //game.documentTypes[documentName];
    
    const folders = parent ? [] : game.folders.filter(f => (f.type === documentName) && f.displayed);
    const label = game.i18n.localize(this.metadata.label);
    const title = game.i18n.format("DOCUMENT.Create", {type: label});

    // Render the document creation form
    const html = await renderTemplate("templates/sidebar/document-create.html", {
      folders,
      name: data.name || game.i18n.format("DOCUMENT.New", {type: label}),
      folder: data.folder,
      hasFolders: folders.length >= 1,
      type: data.type || CONFIG[documentName]?.defaultType || types[0],
      types: types.reduce((obj, t) => {
        const label = CONFIG[documentName]?.typeLabels?.[t] ?? t;
        obj[t] = game.i18n.has(label) ? game.i18n.localize(label) : t;
        return obj;
      }, {}),
      hasTypes: types.length > 1
    });

    // Render the confirmation dialog window
    return Dialog.prompt({
      title: title,
      content: html,
      label: title,
      callback: html => {
        const form = html[0].querySelector("form");
        const fd = new FormDataExtended(form);
        foundry.utils.mergeObject(data, fd.object, {inplace: true});
        if ( !data.folder ) delete data.folder;
        if ( types.length === 1 ) data.type = types[0];
        if ( !data.name?.trim() ) data.name = this.defaultName();
        return this.create(data, {parent, pack, renderSheet: true});
      },
      rejectClose: false,
      options
    });
  }

}
