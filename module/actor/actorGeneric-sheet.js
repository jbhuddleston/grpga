import { system } from "../config.js";
import { BaseActorSheet } from "./baseActor-sheet.js";

/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {BaseActorSheet}
 */
export class ActorGenericSheet extends BaseActorSheet {

  /** @override */
  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      classes: ["grpga", "sheet", "actor"],
      template: "systems/grpga/templates/actor/actorGeneric-sheet.hbs",
      width: 600,
      height: 800,
      tabs: [{ navSelector: ".sheet-nav", contentSelector: ".sheet-body", initial: "combat" }]
    });
  }

  /** @override */
  async getData(options) {
    if (CONFIG.system.testMode) console.debug("entering getData() in ActorGeneric-sheet");

    const context = await super.getData(options);
    // My shortcuts
    const actordata = context.system;
    const traits = actordata.traits;
    const dynamic = actordata.dynamic;
    const tracked = actordata.tracked;
    const headinfo = actordata.headerinfo = {};

    headinfo.level = dynamic.level;
    if (headinfo.level)
      headinfo.level.system.label = system.generic.headerVariables[2];
    headinfo.perception = dynamic.perception;
    headinfo.initiative = dynamic.initiative;
    headinfo.dodge = dynamic.dodge;
    headinfo.xp = dynamic.xp;
    if (headinfo.xp)
      headinfo.xp.system.label = system.generic.headerVariables[1];
    // for d20
    headinfo.hitpoints = tracked.hp;
    headinfo.apr = dynamic.apr;
    headinfo.mpa = dynamic.mpa;
    headinfo.pace = dynamic.pace;
    headinfo.mr = tracked.mr;

    headinfo.race = traits.race;
    headinfo.characterclass = traits.character_class;
    headinfo.specificinfo = traits.specific_information;
    headinfo.additionalinfo = traits.additional_information;
    headinfo.alignment = traits.alignment;

    if (headinfo.alignment)
      headinfo.alignment.system.label = system.generic.headerVariables[0];

    actordata.primaryattributes = [];
    for (let varname of system[context.mode].primaryAttributes) {
      if (!varname.trim()) continue;
      if (dynamic[varname]) actordata.primaryattributes.push(dynamic[varname]);
    }

    if (actordata.useAlternity) {
      actordata.allskills = this.sort(actordata.checks.concat(actordata.skills, actordata.spells));
      for (let item of actordata.allskills) {
        item.system.stepvalue = item.system.moddedvalue - item.system.value;
      }
      actordata.defences = this.sort(actordata.dodges.concat(actordata.parrys));
      for (let item of actordata.defences) {
        item.system.stepvalue = item.system.moddedvalue - item.system.value;
      }
    } else if (actordata.useSineNomine) {
      actordata.defences = this.sort(actordata.dodges.concat(actordata.parrys));
      for (let item of actordata.defences) {
        item.system.value = 16 - item.system.value;
        item.system.moddedvalue = 16 - item.system.moddedvalue;
      }
    }

    // group and sort skill mods
    actordata.checkSkillSpellMods = this.sort(Array.from(new Set(actordata.checkmods.concat(actordata.skillmods, actordata.spellmods))));

    return context;
  }

  async dropData(dragItem) {
    if (CONFIG.system.testMode) console.debug(`processing ${dragItem.type}\n`, dragItem);
  
    super.dropData(dragItem);
  
    switch (dragItem.type) {
      case "critical": {
        const itemdata = {
          name: `${dragItem.condition} [${dragItem.effect}]`,
          type: "Defence",
          system: {
            chartype: this.actor.type,
            category: "block",
            notes: `${dragItem.woundtitle}\n${dragItem.message}`
          }
        }
        let item = await this.actor.createEmbeddedDocuments("Item", [itemdata]);
        const baseitem = await this.actor.items.get(item[0].id);
        baseitem.sheet.render(true);
        break;
      }
    }
  }

}
