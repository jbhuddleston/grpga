class ActorD100System extends foundry.abstract.TypeDataModel {
  /**
   * This is the where the bread and butter of DataModels come into play, the schema definition.
   * It's effectively the replacement for the data you fill in your template.json file.
   * Below is a basic example of how you would define a schema for your ActorD100System class's specific properties,
   * ignoring the 'base' template for this moment.
   */
  /** @override */
  static defineSchema() {
    const fields = foundry.data.fields;
    return {
      /**
       * If you are extending an existing class, you would use the following:
       * ```js
       * ...super.defineSchema(),
       * ```
       *  
       * You can consider adding the following section to your init hook, in which case you can just add the above in any TypeDataModel.
       * Which I would highly recommend.
       * ```js
       * // By default, foundry.abstract.DataModel.defineSchema is coded to throw an error to remind developers to override it.
       * // However, this messes up defineSchema() chaining in template mixins, so we'll scrap that behavior.
       * foundry.abstract.DataModel.defineSchema = () => ({});
       * ```
       * 
       */

      // The following is the minimal schema definition as an example, which would mimic the behavior of your old template.json
      allItems: new fields.SchemaField({
        "Primary-Attribute": new fields.BooleanField({ initial: false }),
        "Container": new fields.BooleanField({ initial: false }),
        "Rollable": new fields.BooleanField({ initial: false }),
        "Trait": new fields.BooleanField({ initial: false }),
        "Melee-Attack": new fields.BooleanField({ initial: false }),
        "Ranged-Attack": new fields.BooleanField({ initial: false }),
        "Defence": new fields.BooleanField({ initial: false }),
        "Pool": new fields.BooleanField({ initial: false }),
        "Modifier": new fields.BooleanField({ initial: false }),
        "Variable": new fields.BooleanField({ initial: false }),
        "Equipment": new fields.BooleanField({ initial: false }),
        "Rank-Progression": new fields.BooleanField({ initial: false }),
        "Hit-Location": new fields.BooleanField({ initial: false }),
        "Wound": new fields.BooleanField({ initial: false })
      })

      /**
       * The following would be an extended schema definition, with additional logic.
       * 'required' is whether this field can be undefined or must have a value
       * 'nullable' is whether this field can be null
       * 
       * ```js
       * allItems: new fields.SchemaField({
       *    "Primary-Attribute": new fields.BooleanField({ required: true, nullable: false, initial: false}),
       *    "Container": new fields.BooleanField({ required: true, nullable: false, initial: false}),
       *    "Rollable": new fields.BooleanField({ required: true, nullable: false, initial: false}),
       *    "Trait": new fields.BooleanField({ required: true, nullable: false, initial: false}),
       *    "Melee-Attack": new fields.BooleanField({ required: true, nullable: false, initial: false}),
       *    "Ranged-Attack": new fields.BooleanField({ required: true, nullable: false, initial: false}),
       *    "Defence": new fields.BooleanField({ required: true, nullable: false, initial: false}),
       *    "Pool": new fields.BooleanField({ required: true, nullable: false, initial: false}),
       *    "Modifier": new fields.BooleanField({ required: true, nullable: false, initial: false}),
       *    "Variable": new fields.BooleanField({ required: true, nullable: false, initial: false}),
       *    "Equipment": new fields.BooleanField({ required: true, nullable: false, initial: false}),
       *    "Rank-Progression": new fields.BooleanField({ required: true, nullable: false, initial: false}),
       *    "Hit-Location": new fields.BooleanField({ required: true, nullable: false, initial: false}),
       *    "Wound": new fields.BooleanField({ required: true, nullable: false, initial: false})
       * })
       * ```
       */
    }
  }

  /**
   * Only one change here, the additional reference to `actordata = this.actor.system` is unnecessary, as `this` is `this.actor.system`.
   */
  /** @override */
  prepareBaseData() {
    this.bm.quarter = Number(actordata.bm.step * 1.5);
    this.bm.half = actordata.bm.step * 2;
    this.bm.move = actordata.bm.step * 3;
    this.bm.sprint = actordata.bm.step * 4;
    this.bm.dash = actordata.bm.step * 5;
    this.dynamic.woundsbleed = { "value": 0 };
    this.dynamic.woundspen = { "value": 0 };
    this.hasStatBonusTable = system.hasStatBonusTable();
  }

  /**
   * Only two 3 changes here on the first 3 lines
   * First renaming `prepareAdditionalData` to `prepareDerivedData` as per foundry design
   * second, actor will be found at `this.parent` instead of `this.actor`, although ofc you could create a getter for this (which is what I personally do and would recommend)
   * and third, `this` is the actordata.
   */
  /** @override */
  prepareDerivedData() {
    const actor = this.parent;
    const actordata = this;
  
    /** Omitting the remainder of prepareDerivedData as it's long and not required for this example */
  }

  /**
   * Omitting your other methods as they are not relevant to the schema definition
   */
}

/**
 * Generally what I'd recommend is for any overlap in data you can do one of two things
 * 1. Standard OOP: Create a Base Model and extend it for each sub-model
 * 2. Use a mixin to add the shared data to each model
 * 
 * I used to use 1 pre-data models, but now I use 2 as it's more flexible and easier to manage imo.
 * 
 * Now, your base model is quite large, so I'm only taking the `bm` section of it, 
 * but the nice thing with mixins is you could totally make multiple mixins for each field, and then combine them as needed.
 */
function hasBmData(baseClass) {
  class TemplateClass extends baseClass {
    /** @override */
    static defineSchema() {
      const fields = foundry.data.fields;
      return {
        ...super.defineSchema(),

        bm: new fields.SchemaField({
          step: new fields.NumberField({ required: true, nullable: false, initial: 0 /*, min: 0, max: 100, step: 1*/ /* No idea what your rules are, but you can define this kind of validation with Data Models ;) */ }),
          quarter: new fields.NumberField({ required: true, nullable: false, initial: 0 }),
          half: new fields.NumberField({ required: true, nullable: false, initial: 0 }),
          move: new fields.NumberField({ required: true, nullable: false, initial: 0 }),
          sprint: new fields.NumberField({ required: true, nullable: false, initial: 0 }),
        })
      }
    }

    /** @override */
    prepareBaseData() {
      super.prepareBaseData();

      // You could define any specific logic for the bm field here, and have it be called by any class that extends this mixin.
      // Same would go for prepareDerivedData
      // Or even something like a _preCreate or _preUpdate method.
    }
  }

  return TemplateClass;
}

/** 
 * At this point you would be able to add it to the ActorD100System class like so:
 */
class ActorD100System extends hasBmData(foundry.abstract.TypeDataModel) {
  // Your class definition here
}

/**
 * If you had multiple mixins it'd look like this:
 */
class ActorD100System extends hasBmData(hasPostureData(foundry.abstract.TypeDataModel)) {
  // Your class definition here
}

/**
 * And if you for example wanted to combine bm/defence/posture and call it something like `hasCombatData` you could do:
 */
function hasCombatData(baseClass) {
  class TemplateClass extends hasBmData(hasPostureData(hasDefenceData(baseClass))) {
    // This is an empty class, to necessitate the combination of the mixins
  }

  return TemplateClass;
}