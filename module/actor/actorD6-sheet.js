import { system } from "../config.js";
import { BaseActorSheet } from "./baseActor-sheet.js";

/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {BaseActorSheet}
 */
export class ActorD6Sheet extends BaseActorSheet {

  /** @override */
  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      classes: ["grpga", "sheet", "actor"],
      template: "systems/grpga/templates/actor/actorD6-sheet.hbs",
      width: 600,
      height: 800,
      tabs: [{ navSelector: ".sheet-nav", contentSelector: ".sheet-body", initial: "combat" }]
    });
  }

  /** @override */
  async getData(options) {
    if (CONFIG.system.testMode) console.debug("entering getData() in ActorD6-sheet");

    const context = await super.getData(options);
    // My shortcuts
    const actordata = context.system;
    const traits = actordata.traits;
    const dynamic = actordata.dynamic;
    const tracked = actordata.tracked;
    const headinfo = actordata.headerinfo = {};

    for (let item of actordata.disadvantages) {
      switch (item?.name) {
        case "Race": headinfo.race = item; break;
        case "Quote": headinfo.quote = item; break;
        case "Personality": headinfo.personality = item; break;
        case "Description": headinfo.description = item; break;
      }
    }
    if (headinfo.race)
      headinfo.race.system.label = system.d6.headerVariables[0];
      if (headinfo.personality)
      headinfo.personality.system.label = system.d6.headerVariables[1];
      if (headinfo.quote)
      headinfo.quote.system.label = system.d6.headerVariables[2];
    headinfo.cp = dynamic.cp;
    if (headinfo.cp)
      headinfo.cp.system.label = system.d6.headerVariables[3];
    headinfo.hp = dynamic.hp;
    if (headinfo.hp)
      headinfo.hp.system.label = system.d6.headerVariables[4];
    if (headinfo.description)
      headinfo.description.system.label = system.d6.headerVariables[5];

    actordata.primaryattributes = [];
    for (let varname of system[context.mode].primaryAttributes) {
      if (!varname.trim()) continue;
      if (dynamic[varname]) {
        actordata.primaryattributes.push(dynamic[varname]);
        dynamic[varname].system.diceandpips = this.actor.toDiceAndPips(dynamic[varname].system.moddedvalue, false);
      }
    }

    for (let skill of actordata.skills) {
      const group = skill.system.group.trim().toLowerCase();
      if (dynamic[group]) {
        skill.system.diceandpips = this.actor.toDiceAndPips(skill.system.moddedvalue + dynamic[group].system.moddedvalue, false);
        skill.system.value += dynamic[group].system.moddedvalue;
        if (dynamic[group].system.skills) {
          if (!dynamic[group].system.skills.includes(skill)) dynamic[group].system.skills.push(skill);
        } else {
          dynamic[group].system.skills = [skill];
        }
      } else {
        // no such primary found
      }
    }
    for (let defence of actordata.defences) {
      defence.system.diceandpips = this.actor.toDiceAndPips(defence.system.moddedvalue, false);
    }
    // group and sort skill mods
    actordata.checkSkillSpellMods = this.sort(Array.from(new Set(actordata.checkmods.concat(actordata.skillmods, actordata.spellmods))));

    // check to see if specific modifiers are toggled so we can set the flags
    // the modifier must affect Variable Formulae, D20 skills or D100 skills to work as a toggle (a modifier of zero is ok)
    // these were designed for D6 Pool ruleset but could be used by others
    for (let item of actordata.checkSkillSpellMods) {
      switch (item?.name) {
        case "Rushed": this.actor.setFlag("grpga", "rushed", item.system.inEffect); break;
        case "Using Edge": this.actor.setFlag("grpga", "usingedge", item.system.inEffect); break;
      }
    }

    return context;
  }

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);
  }

}
