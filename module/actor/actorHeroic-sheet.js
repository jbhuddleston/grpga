import { system } from "../config.js";
import { BaseActorSheet } from "./baseActor-sheet.js";

export class ActorHeroicSheet extends BaseActorSheet {

  /** @override */
  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      classes: ["heroic", "grpga", "sheet", "actor"],
      template: "systems/grpga/templates/actor/actorHeroic-sheet.hbs",
      width: 600,
      height: 800,
      tabs: [{ navSelector: ".sheet-nav", contentSelector: ".sheet-body", initial: "combat" }]
    });
  }

  /** @override */
  async getData(options) {
    if (CONFIG.system.testMode) console.debug("entering getData() in ActorHeroic-sheet");

    const context = await super.getData(options);
    // My shortcuts
    const actordata = context.system;
    const traits = actordata.traits;
    const dynamic = actordata.dynamic;
    const tracked = actordata.tracked;
    const headinfo = actordata.headerinfo = {};

    headinfo.originchoices = system.heroic.originchoices;
    headinfo.sidechoices = system.heroic.sidechoices;
    headinfo.campaignchoices = system.heroic.campaignchoices;
    headinfo.statuschoices = system.heroic.statuschoices;
    headinfo.initiative = dynamic.initiative?.system.moddedvalue || null;
    headinfo.apr = dynamic.apr?.system.value || null;

    actordata.primaryattributes = [];
    for (let varname of system[context.mode].primaryAttributes) {
      if (!varname.trim()) continue;
      if (dynamic[varname]) actordata.primaryattributes.push(dynamic[varname]);
    }

    const valuemap = ["n/a", "FE", "PR", "TY", "GD", "EX", "RM", "IN", "AM", "MG", "UN", "SP", "FN", "LG", "WN", "EP", "CZ"];
    for (const item of actordata.primaryattributes) {
      item.system.display = valuemap[item.system.moddedvalue] + item.system.value;
    }
    for (const item of actordata.defences) {
      item.system.display = valuemap[item.system.value];
    }

    return context;
  }

  async dropData(dragItem) {
    if (CONFIG.system.testMode) console.debug(`processing ${dragItem.type}\n`, dragItem);

    super.dropData(dragItem);

    switch (dragItem.type) {
      case "critical": {
        const itemdata = {
          name: `${dragItem.condition} [${dragItem.effect}]`,
          type: "Defence",
          system: {
            chartype: this.actor.type,
            category: "block",
            notes: `${dragItem.woundtitle}\n${dragItem.message}`
          }
        }
        let item = await this.actor.createEmbeddedDocuments("Item", [itemdata]);
        const baseitem = await this.actor.items.get(item[0].id);
        baseitem.sheet.render(true);
        break;
      }
    }
  }

}
