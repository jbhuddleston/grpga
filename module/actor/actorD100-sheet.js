import { system } from "../config.js";
import { BaseActorSheet } from "./baseActor-sheet.js";

/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {BaseActorSheet}
 */
export class ActorD100Sheet extends BaseActorSheet {

  /** @override */
  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      classes: ["grpga", "sheet", "actor"],
      template: "systems/grpga/templates/actor/actorD100-sheet.hbs",
      width: 600,
      height: 800,
      tabs: [{ navSelector: ".sheet-nav", contentSelector: ".sheet-body", initial: "main" }]
    });
  }

  /** @override */
  async getData(options) {
    if (CONFIG.system.testMode) console.debug("entering getData() in ActorD100-sheet");

    const context = await super.getData(options);
    // My shortcuts
    const actordata = context.system;
    const traits = actordata.traits;
    const dynamic = actordata.dynamic;
    const tracked = actordata.tracked;
    const headinfo = actordata.headerinfo = {};

    headinfo.parry = tracked.parry;
    headinfo.hitpoints = tracked.hp;
    headinfo.woundsbleed = dynamic.woundsbleed;
    headinfo.woundspen = dynamic.woundspen;
    headinfo.encumbrance = dynamic.encumbrance;
    headinfo.defence = actordata.defence;
    headinfo.move = {
      rate: dynamic.move_rate,
      mode: dynamic.move_mode
    };
    if (headinfo.move.mode)
      headinfo.move.mode.system.displayname = headinfo.move.mode.system.label[0];
    headinfo.level = dynamic.level;
    headinfo.experience = dynamic.experience;

    headinfo.race = headinfo.culture = headinfo.profession = {
      _id: "",
      system: {
        notes: "",
        displayname: ""
      }
    };
    for (const item of Object.values(actordata.perks)) {
      if (item.name.startsWith("Culture:")) {
        item.system.displayname = item.name.split(":")[1].trim() || "";
        headinfo.culture = item;
      } else if (item.name.startsWith("Profession:")) {
        item.system.displayname = item.name.split(":")[1].trim() || "";
        headinfo.profession = item;
      } else if (item.name.startsWith("Race:")) {
        item.system.displayname = item.name.split(":")[1].trim() || "";
        headinfo.race = item;
      }
    }

    actordata.primaryattributes = [];
    for (let varname of system[context.mode].primaryAttributes) {
      if (!varname.trim()) continue;
      if (dynamic[varname]) actordata.primaryattributes.push(dynamic[varname]);
    }
    return context;
  }

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    html.find('.importRM').click(this._onImportRMData.bind(this));
  }

  async _onImportRMData(event) {
    event.preventDefault();
    const scriptdata = this.actor.system.itemscript.split(/\r?\n/).map(word => word.trim());

    let biography = this.actor.system.biography;

    for (let entry of scriptdata) {
      const line = entry.split(":");
      switch (line[0].trim()) {
        case "Name": {
          await this.actor.update({ 'name': line[1].trim() });
          break;
        }
        case "Armor Type": { // Rollable: check
          let num = line[1].trim();
          const item = this.actor.system.items.find(i => i.name === "Armor Type");
          const update = { _id: item.id, 'system.formula': num, 'system.notes': entry.trim() };
          await this.actor.updateEmbeddedDocuments("Item", [update]);
          break;
        }
        case "Level": { // Rollable: check
          let num = line[1].trim();
          const item = this.actor.system.items.find(i => i.name.startsWith("Level-Ranks"));
          const update = { _id: item.id, 'system.formula': num, 'system.notes': entry.trim() };
          await this.actor.updateEmbeddedDocuments("Item", [update]);
          break;
        }
        case "MeleeDB": { // Rollable: check
          let num = line[1].trim();
          const item = this.actor.system.items.find(i => i.name === "Melee DB");
          const update = { _id: item.id, 'system.formula': num, 'system.notes': entry.trim() };
          await this.actor.updateEmbeddedDocuments("Item", [update]);
          break;
        }
        case "MissileDB": { // Rollable: check
          let num = line[1].trim();
          const item = this.actor.system.items.find(i => i.name === "Missile DB");
          const update = { _id: item.id, 'system.formula': num, 'system.notes': entry.trim() };
          await this.actor.updateEmbeddedDocuments("Item", [update]);
          break;
        }
        case "Race": { // Rollable: check
          let str = line[1].trim();
          const item = this.actor.system.items.find(i => i.name.startsWith("Race"));
          const update = { _id: item.id, 'name': `Race: ${str}`, 'system.formula': 0, 'system.notes': entry.trim() };
          await this.actor.updateEmbeddedDocuments("Item", [update]);
          break;
        }
        case "Culture": { // Rollable: check
          let str = line[1].trim();
          const item = this.actor.system.items.find(i => i.name.startsWith("Culture"));
          const update = { _id: item.id, 'name': `Culture: ${str}`, 'system.formula': 0, 'system.notes': entry.trim() };
          await this.actor.updateEmbeddedDocuments("Item", [update]);
          break;
        }
        case "Profession": { // Rollable: check
          let str = line[1].trim();
          const item = this.actor.system.items.find(i => i.name.startsWith("Profession"));
          const update = { _id: item.id, 'name': `Profession: ${str}`, 'system.formula': 0, 'system.notes': entry.trim() };
          await this.actor.updateEmbeddedDocuments("Item", [update]);
          break;
        }
        case "Various": {
          let itemdata = {
            type: "Defence",
            system: {
              chartype: "CharacterD100",
            }
          };
          let result = line[1].split("###").map(word => word.trim());
          itemdata.name = result[0];
          itemdata.system.formula = result[1];
          switch (result[2]?.toLowerCase()) {
            case "oe": itemdata.system.category = "block"; break;
            case "oeh": itemdata.system.category = "parry"; break;
            default: itemdata.system.category = "dodge"; break;
          }
          itemdata.system.notes = entry.trim();
          const item = this.actor.system.items.find(i => i.name === result[0]);
          if (item) {
            itemdata._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [itemdata]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [itemdata]);
          }
          break;
        }
        case "Skill":
        case "Spell":
        case "Check": {
          let itemdata = {
            type: "Rollable",
            system: {
              chartype: "CharacterD100",
              category: line[0].toLowerCase(),
            }
          };
          let result = line[1].split("###").map(word => word.trim());
          itemdata.name = result[0];
          itemdata.system.formula = result[1];
          itemdata.system.notes = entry.trim();
          const item = this.actor.system.items.find(i => i.name === result[0]);
          if (item) {
            itemdata._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [itemdata]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [itemdata]);
          }
          break;
        }
        case "Primary": {
          let itemdata = {
            type: "Primary-Attribute",
            system: {
              chartype: "CharacterD100",
            }
          };
          let result = line[1].split("###").map(word => word.trim());
          itemdata.name = result[0];
          itemdata.system.abbr = result[1] || system.slugify(result[0]);
          itemdata.system.attr = Number(result[2]) || 0;
          itemdata.system.notes = entry.trim();
          const item = this.actor.system.items.find(i => i.name === result[0]);
          if (item) {
            itemdata._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [itemdata]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [itemdata]);
          }
          break;
        }
        case "Melee": {
          let itemdata = {
            type: "Melee-Attack",
            system: {
              chartype: "CharacterD100"
            }
          };
          let result = line[1].split("###").map(word => word.trim());
          itemdata.name = result[0];
          itemdata.system.formula = result[1] || 0;
          itemdata.system.armourDiv = Number(result[2]) || 0;
          itemdata.system.weight = Number(result[3]) || 0;
          itemdata.system.damage = result[4];
          itemdata.system.damageType = result[5];
          itemdata.system.minST = result[6] || "oeh";
          itemdata.system.reach = Number(result[7]) || 5;
          itemdata.system.notes = entry.trim();
          const item = this.actor.system.items.find(i => i.name === result[0]);
          if (item) {
            itemdata._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [itemdata]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [itemdata]);
          }
          break;
        }
        case "Ranged": {
          let itemdata = {
            type: "Ranged-Attack",
            system: {
              chartype: "CharacterD100"
            }
          };
          let result = line[1].split("###").map(word => word.trim());
          itemdata.name = result[0];
          itemdata.system.formula = result[1] || 0;
          itemdata.system.armourDiv = Number(result[2]) || 0;
          itemdata.system.accuracy = Number(result[3]) || 0;
          itemdata.system.range = result[4] || "5";
          itemdata.system.damage = result[5];
          itemdata.system.damageType = result[6];
          itemdata.system.rof = Number(result[7]) || 5;
          itemdata.system.minST = result[8] || "oeh";
          itemdata.system.notes = entry.trim();
          const item = this.actor.system.items.find(i => i.name === result[0]);
          if (item) {
            itemdata._id = item.id;
            await this.actor.updateEmbeddedDocuments("Item", [itemdata]);
          } else {
            await this.actor.createEmbeddedDocuments('Item', [itemdata]);
          }
          break;
        }
      }
    }
    await this.actor.update({ 'system.biography': biography });
    await this.actor.update({ 'system.itemscript': '' });
    ui.notifications.info("Stat block import complete");
    return;
  }

  async vsd_onRoll(event) {
    event.preventDefault();
    const dataset = event.currentTarget.dataset;

    const rolldata = {
      actor: this.actor,
      flavour: game.i18n.format(`vsd.phrases.vsd.${dataset.type}`, {
        name: dataset.name,
      }),
      unmodified: Number(dataset.threat) || 0,
      oerange: Number(dataset.oerange) || 5,
      attacktable: dataset.attacktable || "",
      journal: dataset.journal || "",
      crittable: dataset.crittable || "",
      sectable: dataset.sectable || "",
      maxresult: dataset.maxresult || "",
      type: dataset.type,
      modtype: dataset.modtype,
      roll: dataset.roll,
      name: dataset.name,
      id: dataset.itemId,
    };

    this.actor.roll(rolldata);
  }

}
