export class GRPGACard extends Card {

  /* -------------------------------------------- */
  /*  API Methods                                 */
  /* -------------------------------------------- */

  /**
   * Pass this Card to some other Cards document.
   * @param {Cards} to                A new Cards document this card should be passed to
   * @param {object} [options={}]     Options which modify the pass operation
   * @param {object} [options.updateData={}]  Modifications to make to the Card as part of the pass operation,
   *                                  for example the displayed face
   * @returns {Promise<Card>}         A reference to this card after it has been passed to another parent document
   */
  async pass(to, { updateData = {}, ...options } = {}) {
    const created = await this.parent.pass(to, [this.id], { updateData, action: "pass", ...options });
    return created[0];
  }

  /* -------------------------------------------- */

  /**
   * @alias Card#pass
   * @see Card#pass
   * @inheritdoc
   */
  async play(to, { updateData = {}, ...options } = {}) {
    const created = await this.parent.pass(to, [this.id], { updateData, action: "play", ...options });
    return created[0];
  }

  /* -------------------------------------------- */

  /**
   * @alias Card#pass
   * @see Card#pass
   * @inheritdoc
   */
  async discard(to, { updateData = {}, ...options } = {}) {
    const created = await this.parent.pass(to, [this.id], { updateData, action: "discard", ...options });
    return created[0];
  }

  /* -------------------------------------------- */

  /**
   * Create a chat message which displays this Card.
   * @param {object} [messageData={}] Additional data which becomes part of the created ChatMessageData
   * @param {object} [options={}]     Options which modify the message creation operation
   * @returns {Promise<ChatMessage>}  The created chat message
   */
  async toMessage(messageData = {}, options = {}) {
    messageData.content += `<div class="card-draw side-by-side">
      <img class="card-face" src="${this.img}" alt="${this.name}"/>
      </div>`;
    return ChatMessage.implementation.create(messageData, options);
  }
}

export class GRPGACards extends Cards {
  async pass(to, ids, {updateData={}, action="pass", chatNotification=true}={}) {
    const options = {
      updateData: updateData || {},
      action: action || "pass",
      chatNotification: false
    }
    return super.pass(to, ids, options);
  }
}