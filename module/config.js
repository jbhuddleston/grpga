export const system = {
  dataRgx: /[^-(){}\d<>/=*+., DdflorceiMmaxthbsunvpwkxX]/g,
  hasStatBonusTable: () => {
    return game.tables.getName(CONFIG.system.statBonusTableName) != undefined;
  },
  sbs: (stat) => {
    if (game.tables.getName(CONFIG.system.statBonusTableName)) {
      let result = game.tables.getName(CONFIG.system.statBonusTableName).getResultsForRoll(stat);
      return (result.length == 0) ? -100 : Number(result[0].text);
    } else {
      return stat;
    }
  },
  hasStepTable: () => {
    return game.tables.getName(CONFIG.system.stepTableName) != undefined;
  },
  sts: (step) => {
    if (game.tables.getName(CONFIG.system.stepTableName)) {
      let result = game.tables.getName(CONFIG.system.stepTableName).getResultsForRoll(step);
      return (result.length == 0) ? "0" : result[0].text;
    } else {
      return "0";
    }
  },
  slugify: (text) => {
    return text
      .toString() // Cast to string
      .toLowerCase() // Convert the string to lowercase letters
      .normalize("NFD") // The normalize() method returns the Unicode Normalization Form of a given string.
      .trim() // Remove whitespace from both sides of a string
      .replace(/[\s-]+/g, "_") // Replace spaces with underscore
      .replace(/[^\w]+/g, "") // Remove all non-word chars
      .replace(/\_\_+/g, "_"); // Replace multiple underscores with a single one
  },
  entriesAreEqual: (first, second) => {
    if (first.length != second.length) return false;
    for (let i = 0; i < first.length; i++) {
      if (first[i].formula != second[i].formula) return false;
      if (first[i].category != second[i].category) return false;
      if (first[i].targets != second[i].targets) return false;
    }
    return true;
  },
  showJournal: (journalName) => {
    const journalref = system.journalref[system.slugify(journalName)];
    if (journalref) {
      Journal._showEntry(journalref.uuid);
    } else {
      if (journalName)
        ui.notifications.error(
          `There is no matching Journal Entry for ${journalName} in the Compendia.`
        );
    }
  },
  closeJournal: (journalName) => {
    const journalref = system.journalref[system.slugify(journalName)];
    if (journalref.parent) {
      journalref.parent.sheet.close();
    } else {
      journalref.sheet.close();
    }
  },
  combat: {
    defaultActionCost: 1,
    missingInitiative: "You must roll initiative first."
  }
}

system.vsd = {
  combat: [
    { action: "Select", initiative: 90 },
    { action: "Move", initiative: 61 },
    { action: "Cast Prepared Spell", initiative: 52 },
    { action: "Cast Instantaneous Spell", initiative: 51 },
    { action: "Aimed Ranged Attack", initiative: 42 },
    { action: "Throw Ready Weapon", initiative: 41 },
    { action: "Longest Melee Plus", initiative: 36 },
    { action: "Longest Melee", initiative: 35 },
    { action: "Long Melee", initiative: 34 },
    { action: "Short Melee", initiative: 33 },
    { action: "Hand Melee", initiative: 32 },
    { action: "Hand Melee Minus", initiative: 31 },
    { action: "Unaimed Ranged Attack", initiative: 21 },
    { action: "Cast Unprepared Spell", initiative: 11 },
    { action: "Other Action", initiative: 1 },
    { action: "Wait - Ready", initiative: 0 }
  ],
  // skills tab
  oddskills: ["body", "armor"],
  skillsort: [
    { name: "Combat", data: ["blunt", "blades", "ranged", "polearms", "brawl"] },
    { name: "Adventuring", data: ["athletics", "ride", "hunting", "nature", "wandering"] },
    { name: "Roguery", data: ["acrobatics", "stealth", "locks-traps", "perception", "deceive"] },
    { name: "Lore", data: ["arcana", "charisma", "cultures", "healer", "songs-tales"] }
  ],
  skillVariables: ["difficulty", "helpers"],

  // spells tab
  spellVariables: ["spell-range", "attack-spell-range", "prepaim-time", "edb", "mp", "ah1", "ah3", "ah5", "ra5", "tokens"],
  specialrolls: ["spell-failure", "magic-resonance-roll"],
  specialmods: ["critical-severity", "mrr-location", "mrr-spell-type", "weave", "overcasting", "failed-spell-type"],

  // combat tab
  defenceVariables: ["move-mode", "move-rate", "armor-type", "melee-db", "missile-db"],
  savesort: ["willpower-saving-roll", "toughness-saving-roll"],

  defencemodifiers: ["attack-level", "sst", "hp", "drive", "hero"],

  attackVariables: ["missile-range", "prepaim-time", "edb", "parry", "tokens"],

  specialcombatrolls: ["fumble-meleethrown", "fumble-missile"],
  specialcombatmods: ["fumble-mod", "critical-severity"],

  // main tab
  primarysort: {
    name: "Stats",
    data: ["brn", "swi", "for", "wit", "wsd", "bea"]
  },
  advancementpools: ["drive", "hero", "xp", "wl"],

  // stat block import lists of items to remove from the template
  npcremove: [
    "bea", "brn", "for", "swi", "wsd", "wit",
    "blunt", "blades", "polearms", "ranged", "brawl",
    "magic-resonance-roll", "spell-failure", "armor", "body",
    "blades-dagger-pierce", "brawl-dagger-pierce", "brawl-grappling-grapple", "brawl-kick-impact", "brawl-punch-impact",
    "drive", "xp", "hero", "mp", "wl",
    "armor-type", "attack-spell-range", "helpers", "mrr-location", "mrr-spell-type", "move-mode", "ranks-level-1",
    "spell-range", "weave", "critical-grapple", "critical-impact", "critical-pierce", "critical-cut"
  ],
  npcremovemods: [
    "attack-spell-range", "spell-range", "target-is-static", "failed-spell-type",
    "mrr-location-mod", "mrr-spell-type-mod", "weave-mod",
    "bearing-bonus", "brawn-bonus", "swiftness-bonus", "fortitude-bonus", "wisdom-bonus", "wits-bonus",
    "character-attribute-creation", "drive-bonus", "helpers"
  ]
};
system.d100 = {
  primaryAttributes: ["ag", "em", "co", "in", "me", "pr", "re", "qu", "sd", "st"],
  headerVariables: [],
  skillVariables: ["difficulty", "range", "pp"],
  spellVariables: [],
  defenceVariables: ["difficulty"],
  attackVariables: ["pp", "range"],
};
system.d120 = {
  primaryAttributes: ["iq", "ps", "pb", "me", "pp", "spd", "ma", "pe"],
  headerVariables: ["Align", "XP", "Lvl"],
  skillVariables: [],
  spellVariables: [],
  defenceVariables: [],
  attackVariables: [],
};
system.generic = {
  primaryAttributes: ["iq", "ps", "pb", "me", "pp", "spd", "ma", "pe"],
  headerVariables: ["Align", "XP", "Lvl"],
  skillVariables: [],
  spellVariables: [],
  defenceVariables: [],
  attackVariables: [],
};
system.heroic = {
  primaryAttributes: ["fgt", "agi", "str", "end", "rea", "int", "psy", "dyn", "res"],
  headerVariables: ["", "", ""],
  skillVariables: [],
  spellVariables: [],
  defenceVariables: ["difficulty", "extra_actions"],
  attackVariables: [],
  statuschoices: ["Player", "Contact", "Friendly", "Neutral", "Arch Enemy", "Adversary", "Foe", "Minion"],
  originchoices: ["Artificial", "Birth:Legacy", "Birth:Mutant", "Mystical", "Outsider", "Technological", "Trained", "Transformed"],
  campaignchoices: ["Godlike", "World Class", "Mid-Level", "Street"],
  sidechoices: ["Heroic", "Villainous", "Unaligned"],
  universal: {
    "cs+9": { name: "+9CS", blue: 1, white: 2, green: 5, yellow: 30, red: 55 },
    "cs+8": { name: "+8CS", blue: 1, white: 2, green: 10, yellow: 35, red: 60 },
    "cs+7": { name: "+7CS", blue: 1, white: 2, green: 15, yellow: 40, red: 65 },
    "cs+6": { name: "+6CS", blue: 1, white: 2, green: 20, yellow: 45, red: 70 },
    "cs+5": { name: "+5CS", blue: 1, white: 2, green: 25, yellow: 50, red: 75 },
    "cs+4": { name: "+4CS", blue: 1, white: 2, green: 30, yellow: 55, red: 80 },
    "cs+3": { name: "+3CS", blue: 1, white: 2, green: 35, yellow: 60, red: 85 },
    "cs+2": { name: "+2CS", blue: 1, white: 2, green: 40, yellow: 65, red: 90 },
    "cs+1": { name: "+1CS", blue: 1, white: 2, green: 45, yellow: 70, red: 95 },
    "cs+0": { name: "+0CS", blue: 1, white: 2, green: 50, yellow: 75, red: 98 },
    "cs-1": { name: "-1CS", blue: 1, white: 2, green: 55, yellow: 80, red: 100 },
    "cs-2": { name: "-2CS", blue: 1, white: 2, green: 60, yellow: 85, red: 100 },
    "cs-3": { name: "-3CS", blue: 1, white: 2, green: 65, yellow: 90, red: 100 },
    "cs-4": { name: "-4CS", blue: 1, white: 2, green: 70, yellow: 95, red: 100 },
    "cs-5": { name: "-5CS", blue: 1, white: 6, green: 75, yellow: 98, red: 100 },
    "cs-6": { name: "-6CS", blue: 1, white: 11, green: 80, yellow: 98, red: 100 },
    "cs-7": { name: "-7CS", blue: 1, white: 15, green: 85, yellow: 98, red: 100 },
    "cs-8": { name: "-8CS", blue: 1, white: 15, green: 90, yellow: 98, red: 100 },
    "cs-9": { name: "-9CS", blue: 1, white: 20, green: 95, yellow: 98, red: 100 },
  },
  outcomes: {
    blindsiding: { blue: "R/C", white: "No", green: "+1CS", yellow: "+2CS", red: "+4CS" },
    blocking: { blue: "+Stun", white: "-4RS", green: "-2RS", yellow: "Strength", red: "+1RS" },
    blunt_attack: { blue: "Fumble", white: "Miss", green: "Hit", yellow: "Slam", red: "Stun" },
    bounce_back: { blue: "+Stun", white: "No", green: "End -1RS", yellow: "End", red: "End +1RS" },
    catching: { blue: "+Stun", white: "No", green: "Partial", yellow: "Damage", red: "Catch" },
    charging: { blue: "+Stun", white: "Miss", green: "Hit", yellow: "Slam", red: "Stun" },
    dodging: { blue: "+Stun", white: "No", green: "-1CS", yellow: "-2CS", red: "Dodge" },
    dynamic_attack: { blue: "Mock", white: "No", green: "-1CS", yellow: "-2CS", red: "-4CS" },
    edged_attack: { blue: "Fumble", white: "Miss", green: "Hit", yellow: "Stun", red: "Kill" },
    energy: { blue: "Fumble", white: "Miss", green: "Hit", yellow: "Bullseye", red: "Kill" },
    escaping: { blue: "+Stun", white: "No", green: "Partial", yellow: "Escape", red: "Reverse" },
    evading: { blue: "+Stun", white: "No", green: "-1CS", yellow: "-2CS", red: "Evade" },
    feint: { blue: "Fumble", white: "No", green: "+1CS", yellow: "+2CS", red: "+4CS" },
    force: { blue: "Fumble", white: "Miss", green: "Hit", yellow: "Bullseye", red: "Stun" },
    grabbing: { blue: "Shatter", white: "No", green: "Take", yellow: "Grab", red: "Break" },
    grappling: { blue: "+Stun", white: "No", green: "Partial", yellow: "Grapple", red: "Sub" },
    interposing: { blue: "+Critical", white: "No", green: "-1CS", yellow: "-2CS", red: "Interpose" },
    lure: { blue: "Fumble", white: "No", green: "-2CS", yellow: "-1CS", red: "+Blindside" },
    roll_with_the_blow: { blue: "+1RS", white: "No", green: "-1RS", yellow: "-2RS", red: "-4RS" },
    shooting: { blue: "Fumble", white: "Miss", green: "Hit", yellow: "Bullseye", red: "Kill" },
    throwing_blunt: { blue: "Fumble", white: "Miss", green: "Hit", yellow: "Bullseye", red: "Stun" },
    throwing_edged: { blue: "Fumble", white: "Miss", green: "Hit", yellow: "Bullseye", red: "Kill" },
    resist_slam: { blue: "+Stun", white: "Grand Slam", green: "1 Area", yellow: "Stagger", red: "No" },
    resist_stun: { blue: "+KO", white: "1-10", green: "1", yellow: "Daze", red: "No" },
    resist_kill: { blue: "KIA", white: "End Loss", green: "E/S", yellow: "Injury", red: "No" },
    resist_ko: { blue: "+Kill?", white: "Knockout", green: "+Stun", yellow: "Daze", red: "No" },
    surprise: { blue: "+Blindside", white: "Yes", green: "Go Last", yellow: "Startle", red: "No" },
    response: { blue: "Hostile", white: "Negative", green: "Neutral", yellow: "Positive", red: "Friendly" }
  }
};
system.d6 = {
  primaryAttributes: ["mgt", "agl", "wit", "cha"],
  headerVariables: ["Race", "Personality", "Quote", "CP", "HP", "Descr"],
  skillVariables: [],
  spellVariables: [],
  defenceVariables: ["wound_level", "actions"],
  attackVariables: ["arrows"],
};
system["3d6"] = {
  primaryAttributes: ["st", "dx", "iq", "ht", "qu"],
  headerVariables: ["Align", "XP", "Lvl"],
  skillVariables: [],
  spellVariables: [],
  defenceVariables: [],
  attackVariables: [],
};
system.mnm = {
  primaryAttributes: ["str", "sta", "agl", "dex", "fgt", "int", "awe", "pre"],
  headerVariables: [],
  skillVariables: ["ranks_skills"],
  spellVariables: [],
  defenceVariables: ["ranks_defenses"],
  attackVariables: [],
  trainedonlyskills: { "Acrobatics": true, "Expertise": true, "Investigation": true, "Sleight of Hand": true, "Treatment": true, "Technology": true, "Vehicles": true }
};

CONFIG.ChatMessage.template = "systems/grpga/templates/chat/chat-message.hbs";

CONFIG.postures = [
  "systems/grpga/icons/postures/standing.png",
  "systems/grpga/icons/postures/sitting.png",
  "systems/grpga/icons/postures/crouching.png",
  "systems/grpga/icons/postures/crawling.png",
  "systems/grpga/icons/postures/kneeling.png",
  "systems/grpga/icons/postures/lyingback.png",
  "systems/grpga/icons/postures/lyingprone.png",
  "systems/grpga/icons/postures/sittingchair.png"
];

CONFIG.sizemods = [
  "systems/grpga/icons/sizemods/smneg1.png",
  "systems/grpga/icons/sizemods/smneg2.png",
  "systems/grpga/icons/sizemods/smneg3.png",
  "systems/grpga/icons/sizemods/smneg4.png",
  "systems/grpga/icons/sizemods/smpos1.png",
  "systems/grpga/icons/sizemods/smpos2.png",
  "systems/grpga/icons/sizemods/smpos3.png",
  "systems/grpga/icons/sizemods/smpos4.png"
];

CONFIG.crippled = [
  "systems/grpga/icons/crippled/crippledleftarm.png",
  "systems/grpga/icons/crippled/crippledlefthand.png",
  "systems/grpga/icons/crippled/crippledleftleg.png",
  "systems/grpga/icons/crippled/crippledleftfoot.png",
  "systems/grpga/icons/crippled/crippledrightarm.png",
  "systems/grpga/icons/crippled/crippledrighthand.png",
  "systems/grpga/icons/crippled/crippledrightleg.png",
  "systems/grpga/icons/crippled/crippledrightfoot.png",
];

CONFIG.statusEffects3d6 = [
  { img: 'systems/grpga/icons/postures/standing.png', id: 'standing', name: 'local.postures.standing' },
  { img: 'systems/grpga/icons/postures/sitting.png', id: 'sitting', name: 'local.postures.sitting' },
  { img: 'systems/grpga/icons/postures/crouching.png', id: 'crouching', name: 'local.postures.crouching' },
  { img: 'systems/grpga/icons/postures/crawling.png', id: 'crawling', name: 'local.postures.crawling' },
  { img: 'systems/grpga/icons/postures/kneeling.png', id: 'kneeling', name: 'local.postures.kneeling' },
  { img: 'systems/grpga/icons/postures/lyingback.png', id: 'lyingback', name: 'local.postures.proneb' },
  { img: 'systems/grpga/icons/postures/lyingprone.png', id: 'lyingprone', name: 'local.postures.pronef' },
  { img: 'systems/grpga/icons/postures/sittingchair.png', id: 'sittingchair', name: 'local.postures.sitting' },
  { img: 'systems/grpga/icons/conditions/shock1.png', id: 'shock1', name: 'local.conditions.shock' },
  { img: 'systems/grpga/icons/conditions/shock2.png', id: 'shock2', name: 'local.conditions.shock' },
  { img: 'systems/grpga/icons/conditions/shock3.png', id: 'shock3', name: 'local.conditions.shock' },
  { img: 'systems/grpga/icons/conditions/shock4.png', id: 'shock4', name: 'local.conditions.shock' },
  { img: 'systems/grpga/icons/conditions/reeling.png', id: 'reeling', name: 'local.conditions.reeling' },
  { img: 'systems/grpga/icons/conditions/tired.png', id: 'tired', name: 'local.conditions.tired' },
  { img: 'systems/grpga/icons/conditions/collapse.png', id: 'collapse', name: 'local.conditions.collapse' },
  { img: 'systems/grpga/icons/conditions/unconscious.png', id: 'unconscious', name: 'local.conditions.unconscious' },
  { img: 'systems/grpga/icons/conditions/minus1xhp.png', id: 'minushp1', name: 'local.conditions.minushp' },
  { img: 'systems/grpga/icons/conditions/minus2xhp.png', id: 'minushp2', name: 'local.conditions.minushp' },
  { img: 'systems/grpga/icons/conditions/minus3xhp.png', id: 'minushp3', name: 'local.conditions.minushp' },
  { img: 'systems/grpga/icons/conditions/minus4xhp.png', id: 'minushp4', name: 'local.conditions.minushp' },
  { img: 'systems/grpga/icons/conditions/stunned.png', id: 'stunned', name: 'local.conditions.stunned' },
  { img: 'systems/grpga/icons/conditions/surprised.png', id: 'surprised', name: 'local.conditions.surprised' },
  { img: 'systems/grpga/icons/defeated.png', id: 'dead', name: 'local.conditions.defeated' },
  { img: 'systems/grpga/icons/blank.png', id: 'none', name: 'local.conditions.none' },
  { img: 'systems/grpga/icons/stances/hth.svg', id: 'hth', name: 'local.stances.hth' },
  { img: 'systems/grpga/icons/stances/magic.svg', id: 'magic', name: 'local.stances.magic' },
  { img: 'systems/grpga/icons/stances/ranged.svg', id: 'ranged', name: 'local.stances.ranged' },
  { img: 'systems/grpga/icons/stances/thrown.svg', id: 'thrown', name: 'local.stances.thrown' },
  { img: 'systems/grpga/icons/crippled/crippledleftarm.png', id: 'crippledleftarm', name: 'local.conditions.crippled' },
  { img: 'systems/grpga/icons/crippled/crippledlefthand.png', id: 'crippledlefthand', name: 'local.conditions.crippled' },
  { img: 'systems/grpga/icons/crippled/crippledleftleg.png', id: 'crippledleftleg', name: 'local.conditions.crippled' },
  { img: 'systems/grpga/icons/crippled/crippledleftfoot.png', id: 'crippledleftfoot', name: 'local.conditions.crippled' },
  { img: 'systems/grpga/icons/crippled/crippledrightarm.png', id: 'crippledrightarm', name: 'local.conditions.crippled' },
  { img: 'systems/grpga/icons/crippled/crippledrighthand.png', id: 'crippledrighthand', name: 'local.conditions.crippled' },
  { img: 'systems/grpga/icons/crippled/crippledrightleg.png', id: 'crippledrightleg', name: 'local.conditions.crippled' },
  { img: 'systems/grpga/icons/crippled/crippledrightfoot.png', id: 'crippledrightfoot', name: 'local.conditions.crippled' },
  { img: 'systems/grpga/icons/sizemods/smneg1.png', id: 'smaller1', name: 'local.conditions.smaller' },
  { img: 'systems/grpga/icons/sizemods/smneg2.png', id: 'smaller2', name: 'local.conditions.smaller' },
  { img: 'systems/grpga/icons/sizemods/smneg3.png', id: 'smaller3', name: 'local.conditions.smaller' },
  { img: 'systems/grpga/icons/sizemods/smneg4.png', id: 'smaller4', name: 'local.conditions.smaller' },
  { img: 'systems/grpga/icons/sizemods/smpos1.png', id: 'larger1', name: 'local.conditions.larger' },
  { img: 'systems/grpga/icons/sizemods/smpos2.png', id: 'larger2', name: 'local.conditions.larger' },
  { img: 'systems/grpga/icons/sizemods/smpos3.png', id: 'larger3', name: 'local.conditions.larger' },
  { img: 'systems/grpga/icons/sizemods/smpos4.png', id: 'larger4', name: 'local.conditions.larger' }
];
CONFIG.statusEffectsmnm = CONFIG.statusEffects3d6;
CONFIG.statusEffectsd120 = CONFIG.statusEffects3d6;
CONFIG.statusEffectsgeneric = CONFIG.statusEffects3d6;
CONFIG.statusEffectsheroic = CONFIG.statusEffects3d6;
CONFIG.statusEffectsd6 = CONFIG.statusEffects3d6;
CONFIG.statusEffectsd100 = CONFIG.statusEffects3d6;

CONFIG.controlIcons.defeated = "systems/grpga/icons/defeated.png";

CONFIG.JournalEntry.noteIcons = {
  "Marker": "systems/grpga/icons/buildings/point_of_interest.png",
  "Apothecary": "systems/grpga/icons/buildings/apothecary.png",
  "Beastmen Herd 1": "systems/grpga/icons/buildings/beastmen_camp1.png",
  "Beastmen Herd 2": "systems/grpga/icons/buildings/beastmen_camp2.png",
  "Blacksmith": "systems/grpga/icons/buildings/blacksmith.png",
  "Bretonnian City 1": "systems/grpga/icons/buildings/bret_city1.png",
  "Bretonnian City 2": "systems/grpga/icons/buildings/bret_city2.png",
  "Bretonnian City 3": "systems/grpga/icons/buildings/bret_city3.png",
  "Bretonnian Worship": "systems/grpga/icons/buildings/bretonnia_worship.png",
  "Caste Hill 1": "systems/grpga/icons/buildings/castle_hill1.png",
  "Caste Hill 2": "systems/grpga/icons/buildings/castle_hill2.png",
  "Caste Hill 3": "systems/grpga/icons/buildings/castle_hill3.png",
  "Castle Wall": "systems/grpga/icons/buildings/castle_wall.png",
  "Cave 1": "systems/grpga/icons/buildings/cave1.png",
  "Cave 2": "systems/grpga/icons/buildings/cave2.png",
  "Cave 3": "systems/grpga/icons/buildings/cave3.png",
  "Cemetery": "systems/grpga/icons/buildings/cemetery.png",
  "Chaos Portal": "systems/grpga/icons/buildings/chaos_portal.png",
  "Chaos Worship": "systems/grpga/icons/buildings/chaos_worship.png",
  "Court": "systems/grpga/icons/buildings/court.png",
  "Dwarf Beer": "systems/grpga/icons/buildings/dwarf_beer.png",
  "Dwarf Hold 1": "systems/grpga/icons/buildings/dwarf_hold1.png",
  "Dwarf Hold 2": "systems/grpga/icons/buildings/dwarf_hold2.png",
  "Dwarf Hold 3": "systems/grpga/icons/buildings/dwarf_hold3.png",
  "Empire Barracks": "systems/grpga/icons/buildings/empire_barracks.png",
  "Empire City 1": "systems/grpga/icons/buildings/empire_city1.png",
  "Empire City 2": "systems/grpga/icons/buildings/empire_city2.png",
  "Empire City 3": "systems/grpga/icons/buildings/empire_city3.png",
  "Farm": "systems/grpga/icons/buildings/farms.png",
  "Food": "systems/grpga/icons/buildings/food.png",
  "Guard Post": "systems/grpga/icons/buildings/guards.png",
  "Haunted Hill": "systems/grpga/icons/buildings/haunted_hill.png",
  "Haunted Wood": "systems/grpga/icons/buildings/haunted_wood.png",
  "Inn 1": "systems/grpga/icons/buildings/inn1.png",
  "Inn 2": "systems/grpga/icons/buildings/inn2.png",
  "Kislev City 1": "systems/grpga/icons/buildings/kislev_city1.png",
  "Kislev City 2": "systems/grpga/icons/buildings/kislev_city2.png",
  "Kislev City 3": "systems/grpga/icons/buildings/kislev_city3.png",
  "Lumber": "systems/grpga/icons/buildings/lumber.png",
  "Magic": "systems/grpga/icons/buildings/magic.png",
  "Metal": "systems/grpga/icons/buildings/metal.png",
  "Mountain 1": "systems/grpga/icons/buildings/mountains1.png",
  "Mountain 2": "systems/grpga/icons/buildings/mountains2.png",
  "Orcs": "systems/grpga/icons/buildings/orcs.png",
  "Orc Camp": "systems/grpga/icons/buildings/orc_city.png",
  "Port": "systems/grpga/icons/buildings/port.png",
  "Road": "systems/grpga/icons/buildings/roads.png",
  "Ruins": "systems/grpga/icons/buildings/ruins.png",
  "Scroll": "systems/grpga/icons/buildings/scroll.png",
  "Sigmar": "systems/grpga/icons/buildings/sigmar_worship.png",
  "Stables": "systems/grpga/icons/buildings/stables.png",
  "Standing Stones": "systems/grpga/icons/buildings/standing_stones.png",
  "Swamp": "systems/grpga/icons/buildings/swamp.png",
  "Temple": "systems/grpga/icons/buildings/temple.png",
  "Textile": "systems/grpga/icons/buildings/textile.png",
  "Tower 1": "systems/grpga/icons/buildings/tower1.png",
  "Tower 2": "systems/grpga/icons/buildings/tower2.png",
  "Tower Hill": "systems/grpga/icons/buildings/tower_hill.png",
  "Wizard Tower": "systems/grpga/icons/buildings/wizard_tower.png",
  "Ulric": "systems/grpga/icons/buildings/ulric_worship.png",
  "Village 1": "systems/grpga/icons/buildings/village1.png",
  "Village 2": "systems/grpga/icons/buildings/village2.png",
  "Village 3": "systems/grpga/icons/buildings/village3.png",
  "Wood Elves 1": "systems/grpga/icons/buildings/welves1.png",
  "Wood Elves 2": "systems/grpga/icons/buildings/welves2.png",
  "Wood Elves 3": "systems/grpga/icons/buildings/welves3.png"
};
CONFIG.tokenEffects = {
  effectSize: {
    xLarge: 2,
    large: 3,
    medium: 4,
    small: 5
  },
  effectSizeChoices: {
    small: "Small (Default) - 5x5",
    medium: "Medium - 4x4",
    large: "Large - 3x3",
    xLarge: "Extra Large - 2x2"
  }
};
