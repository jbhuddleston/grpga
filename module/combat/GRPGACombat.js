import { system } from "../config.js";

export class GRPGACombat extends Combat {

  async nextRound() {
    this.combatants.forEach(c => c.actor.resetModVars(true));
    return super.nextRound();
  }
}
export class BBFCombat extends Combat {

  async nextRound() {
    this.combatants.forEach(c => c.actor.resetModVars(true));
    await this.resetAll();
    await this.rollAll();
    return super.nextRound();
  }
}
