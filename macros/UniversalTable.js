const buttondata = [
  { label: "+9CS" },
  { label: "+8CS" },
  { label: "+7CS" },
  { label: "+6CS" },
  { label: "+5CS" },
  { label: "+4CS" },
  { label: "+3CS" },
  { label: "+2CS" },
  { label: "+1CS" },
  { label: "+0CS" },
  { label: "-1CS" },
  { label: "-2CS" },
  { label: "-3CS" },
  { label: "-4CS" },
  { label: "-5CS" },
  { label: "-6CS" },
  { label: "-7CS" },
  { label: "-8CS" },
  { label: "-9CS" }
];

const buttons = {};
for (const button of buttondata) {
  buttons[button.label] = {
    label: button.label,
    callback: () => {
      label = button.label;
      d.render(true);
    }
  }
}

let d = new Dialog({
  title: `Universal Table`,
  buttons: buttons,
  close: async html => {
    if (label) {
      const tableuuid = await game.packs.get(`world.universal-table`).index.getName(label).uuid;
      const table = await fromUuid(tableuuid);
      const red = table.results.contents[4].range[0];
      const yellow = table.results.contents[3].range[0];
      const green = table.results.contents[2].range[0];
      const white = table.results.contents[1].range[0];
      console.log(table);
      const results = await table.drawMany(1, { displayChat: false });
      const total = results.roll.total;
      const result = results.results[0];
      console.log(results);
      const text = result.text;
      let background = "white";
      const tored = Math.max(red-total, -1);
      const toyellow = Math.max(yellow-total, -1);
      const togreen = Math.max(green-total, -1);
      const towhite = Math.max(white-total, -1);

      switch (text) {
        case "Blue":{
          background = "#8888ff";
          break;
        }
        case "Green":{
          background = "#66ff66";
          break;
        }
        case "Yellow":{
          background = "yellow";
          break;
        }
        case "Red":{
          background = "red";
          break;
        }
        default: {

        }
      }
      let content = `<div>Rolled ${total} on the: <b>${table.name}</b> column</div>`;
      content += `<div style="background-color:${background}">${text} Result</div>`;
      if (towhite > 0) content += `<div style="background-color:white">To White: ${towhite} Karma</div>`;
      if (togreen > 0) content += `<div style="background-color:#66ff66">To Green: ${togreen} Karma</div>`;
      if (toyellow > 0) content += `<div style="background-color:yellow">To Yellow: ${toyellow} Karma</div>`;
      if (tored > 0) content += `<div style="background-color:red">To Red: ${tored} Karma</div>`;

      const alias = canvas.tokens.controlled[0]?.name || game.users.current.name;
      await ChatMessage.create({
        speaker: { alias: alias },
        content: content
      });
      label = "";
    }
  }
},
  {
    width: 200,
    classes: ["mydialog"],
    top: 0,
    left: 0
  });
d.render(true);
