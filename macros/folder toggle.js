/**
 * This will bring up a dialog containing the names of all the conditions in 
 * the specified items folder. They must all be modifiers that may be toggled 
 * and you use that dialog to toggle the effect on selected tokens. This macro 
 * will copy the condition to the token before toggling it so you will always 
 * apply the most recent version of the condition.
 */

const folderName = "conditions";

/**
 * @folderName is the case-sensitive name of the folder containing your conditions
 */
async function toggleFolderConditions(folderName) {
  const folder = game.collections.get("Folder").getName(folderName);
  if (folder == undefined) {
    ui.notifications.error(`The folder <b>${folderName}</b> was not found.`);
    return;
  }

  const items = folder.contents;

  const buttons = {};
  for (const button of items) {
    buttons[button.name] = {
      label: button.name,
      buttonData: button,
      callback: () => {
        (label = button.name), (icon = button.img), d.render(true);
      },
    };
  }

  let d = new Dialog(
    {
      title: `Toggle Status Effect`,
      buttons: buttons,
      close: async (html) => {
        if (icon) {

          const tokens = canvas.tokens.controlled;
          if (tokens.length == 0) {
            ui.notifications.error("Please select at least one token first");
            return;
          }
          for (const token of tokens) {
            await token.toggleEffect({
              icon: icon,
              id: label,
              label: label,
            });
            const actor = token.actor;
            const olditems = [];
            const itemdata = buttons[label].buttonData;
            // set the chartype to the type of the actor
            itemdata.system.chartype = actor.type;
            // if there is already an item of this name and type, delete it
            const existing = actor.items.filter((item) => item.name == itemdata.name);
            while (existing[0]) {
              const current = existing.pop();
              if (current.type == itemdata.type) olditems.push(current.id);
            }
            await actor.deleteEmbeddedDocuments("Item", olditems);

            const inEffect = actor.effects.find((i) => i.name == label) != undefined;
            if (inEffect) {
               await actor.createEmbeddedDocuments("Item", [itemdata], { renderSheet: false, });
               //const newitem = actor.items.filter((item) => item.name == itemdata.name)[0];
               //await actor.updateEmbeddedDocuments("Item", [{ _id: newitem._id, "system.inEffect": true },]);
            }
          }
          label = "";
          icon = undefined;
        }
      },
    },
    {
      width: 200,
      classes: ["mydialog"],
      top: 0,
      left: 0,
    }
  );
  d.render(true);
}
toggleFolderConditions(folderName);
console.log(folderName);