/**
 * Select the items folder from which you want to obtain 
 * the items to copy to your selected token actor.
 */
 async function fetch() {
    const itemFolders = game.collections.get("Folder").filter(c => { return c.type == "Item" });
    const buttons = {};
    for (const button of itemFolders) {
        buttons[button.name] = {
            label: button.name,
            callback: () => {
                applyChanges = true;
                label = button.name
            }
        }
    }
    let applyChanges = false;
    new Dialog({
        title: `Copy/Update Token Items from Folder`,
        buttons: buttons,
        close: async html => {
            if (applyChanges) {
                main(label);
            }
        }
    },
    {
        classes: ["vsddialog"],
        width: 175,
        top: 0,
        left: 0
    }).render(true);
}

async function main(folderName) {

    if (canvas.tokens.controlled.length == 0) {
        ui.notifications.error("Please select at least one token first");
        return;
    }

    const items = game.collections.get("Folder").getName(folderName).contents;
    canvas.tokens.controlled.forEach(async function (token) {
        const actor = token.actor;
        const newitems = [];
        const olditems = [];
        items.forEach(function (item) {
            const itemdata = item.system;
            // set the chartype to the type of the actor
            itemdata.system.chartype = actor.type;
            // add the item to the array of those to be added to the actor
            newitems.push(itemdata);
            // if there is already an item of this name and type, delete it
            const existing = actor.items.filter(item => item.name == itemdata.name);
            while (existing[0]) {
                const current = existing.pop();
                if (current.type == itemdata.type) olditems.push(current.id)
            }
        });
        await actor.deleteEmbeddedDocuments('Item', olditems);
        await actor.createEmbeddedDocuments('Item', newitems, { renderSheet: false });
    });
    ui.notifications.info(`Copies and Updates completed successfully.`,);

}
fetch();