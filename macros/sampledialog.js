
let d = new Dialog({
    title: "Test Dialog",
    content: "<p>You must choose either Option 1, or Option 2</p>",
    buttons: {
        one: {
            icon: '<i class="fas fa-check"></i>',
            label: "Option One",
            callback: () => d.render(true)
        },
        two: {
            icon: '<i class="fas fa-times"></i>',
            label: "Option Two",
            callback: () => console.debug("Chose Two")
        }
    },
    default: "two",
    render: html => console.debug("Register interactivity in the rendered dialog"),
    close: html => console.debug("This always is logged no matter which option is chosen")
});
d.render(true);
