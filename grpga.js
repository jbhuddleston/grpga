// Import Modules
import { BaseActor } from "./module/actor/baseActor.js";
import { Actor3D6Sheet } from "./module/actor/actor3D6-sheet.js";
import { ActorGenericSheet } from "./module/actor/actorGeneric-sheet.js";
import { Actor3D6FAHRSheet } from "./module/actor/actor3D6FAHR-sheet.js";
import { ActorMnMSheet } from "./module/actor/actorMnM-sheet.js";
import { ActorD100Sheet } from "./module/actor/actorD100-sheet.js";
import { ActorD120Sheet, ActorPalladiumSheet } from "./module/actor/actorD120-sheet.js";
import { ActorHeroicSheet } from "./module/actor/actorHeroic-sheet.js";
import { ActorD6Sheet } from "./module/actor/actorD6-sheet.js";
import { BaseItem } from "./module/item/item.js";
import { BaseItemSheet, PoolItemSheet, ModifierItemSheet, VariableItemSheet, ContainerItemSheet, TraitItemSheet } from "./module/item/item-sheet.js";
import { system } from "./module/config.js";
import * as FANSpeedProvider from './module/speed-provider.js';
import { GRPGATokenDocument, GRPGAToken, TokenEffects } from "./module/token.js";
import { GRPGACard, GRPGACards } from "./module/card.js";
import { GRPGACombat } from "./module/combat/GRPGACombat.js";
import { ActionPointCombat, ActionPointCombatTracker, ActionPointCombatant, ActionPointCombatantConfig } from "./module/combat/actionPointCombat.js";
import { MyDialog } from "./module/dialog.js";

// Pre-load templates
async function preloadHandlebarsTemplates() {
  const templatePaths = [
    "systems/grpga/templates/partials/gmod.hbs",
    "systems/grpga/templates/partials/modifiers.hbs",
    "systems/grpga/templates/partials/sectiontitles.hbs"
  ];
  return loadTemplates(templatePaths);
};

Roll.CHAT_TEMPLATE = "systems/grpga/templates/dice/roll.hbs";
Roll.TOOLTIP_TEMPLATE = "systems/grpga/templates/dice/tooltip.hbs";

Hooks.once('init', () => {

  CONFIG.system = system;
  FANSpeedProvider.init()
  // Define custom Entity classes
  CONFIG.Actor.documentClass = BaseActor;
  CONFIG.Item.documentClass = BaseItem;
  CONFIG.Token.documentClass = GRPGATokenDocument;
  CONFIG.Token.objectClass = GRPGAToken;
  CONFIG.Card.documentClass = GRPGACard;
  CONFIG.Cards.documentClass = GRPGACards;

  // Enable the modulus modifier for dice terms
  CONFIG.Dice.terms.d.MODIFIERS.mod = function () {
    for (let r of this.results) {
      r.result = r.result % this.faces;
    }
  };

  // Roll a merged exploding die (all explosions merged into a single result)
  CONFIG.Dice.terms.d.MODIFIERS.xx = async function (modifier) {
    // Match xx, xx9, xx>9, xx2>9
    const rgx = /xx([0-9]+)?([<>=]+)?([0-9]+)?/i;
    const match = modifier.match(rgx);
    if (!match) return false;
    let [depth, comparison, target] = match.slice(1);

    // If no comparison or target are provided, treat the depth as the target value
    if (depth && !(target || comparison)) {
      target = depth;
      depth = null;
    }

    // Determine target values
    target = Number.isNumeric(target) ? parseInt(target) : this.faces;
    comparison = comparison || ">=";

    // Determine the number of allowed explosions
    depth = Number.isNumeric(depth) ? parseInt(depth) : 100;

    // Recursively explode until there are no remaining results to explode
    let current = 0;
    let newresults = [];

    // only cycle through the original results
    const initial = this.results.length;
    while (current < initial) {
      let thisdepth = depth; // allow current result to explode depth times
      let r = this.results[current];
      newresults.push(r);
      current++;
      if (!r.active) continue;

      // Determine whether to explode this result
      if (foundry.dice.terms.DiceTerm.compareResult(r.result, comparison, target)) {
        r.exploded = true;
        let discard = { ...r };
        discard.discarded = true;
        discard.active = false;
        newresults.push(discard);
        let newrollindex = this.results.length;
        await this.roll();
        thisdepth--;
        let newresult = this.results[newrollindex];
        newresult.discarded = true;
        newresult.active = false;
        newresults.push(newresult);
        r.result += newresult.result;
        while (foundry.dice.terms.DiceTerm.compareResult(newresult.result, comparison, target) && thisdepth > 0) {
          newresult.exploded = true;
          newrollindex++;
          await this.roll();
          thisdepth--;
          newresult = this.results[newrollindex];
          newresult.discarded = true;
          newresult.active = false;
          newresults.push(newresult);
          r.result += newresult.result;
        }
      }
    }
    this.results = newresults;
  };

  // Roll an Open-Ended die
  CONFIG.Dice.terms.d.MODIFIERS.oe = async function (modifier) {
    // Match oe2<5>96, oe2=5, oe5, oe
    const rgx = /oe([0-9]+)?([<>=]+)?([0-9]+)?([<>]+)?([0-9]+)?/i;
    const match = modifier.match(rgx);
    if (!match) return false;
    let [depth, lowcomp, lowtarget, highcomp, hightarget] = match.slice(1);

    // set the default range
    let range = Math.ceil(this.faces / 20);

    // If there is no depth, set it to the default range
    depth = Number.isNumeric(depth) ? parseInt(depth) : range;

    // If no comparison or target are provided, use the default range
    if (depth && !(lowtarget || lowcomp)) {
      lowtarget = depth + 1;
      hightarget = this.faces - depth;
      depth = null;
    }
    // Determine lowtarget values
    lowtarget = Number.isNumeric(lowtarget) ? parseInt(lowtarget) : range + 1;

    if (depth && !(hightarget || highcomp)) {
      if (lowcomp == "=") {
        lowtarget += 1;
      }
      hightarget = this.faces - lowtarget + 1;
    }
    // Determine hightarget values
    hightarget = Number.isNumeric(hightarget) ? parseInt(hightarget) : this.faces - range;

    // Recursively explode until there are no remaining results to explode
    let current = 0;
    let newresults = [];
    if (depth == null) depth = 100;

    // only cycle through the original results
    const initial = this.results.length;
    while (current < initial) {
      let thisdepth = depth; // allow current result to explode depth times
      let r = this.results[current];
      newresults.push(r);
      current++;
      if (!r.active) continue;

      // Determine whether to explode this result
      if (foundry.dice.terms.DiceTerm.compareResult(r.result, ">", hightarget)) {
        r.exploded = true;
        let discard = { ...r };
        discard.discarded = true;
        discard.active = false;
        newresults.push(discard);
        let newrollindex = this.results.length;
        await this.roll();
        thisdepth--;
        let newresult = this.results[newrollindex];
        newresult.discarded = true;
        newresult.active = false;
        newresults.push(newresult);
        r.result += newresult.result;
        while (foundry.dice.terms.DiceTerm.compareResult(newresult.result, ">", hightarget) && thisdepth > 0) {
          newresult.exploded = true;
          newrollindex++;
          await this.roll();
          thisdepth--;
          newresult = this.results[newrollindex];
          newresult.discarded = true;
          newresult.active = false;
          newresults.push(newresult);
          r.result += newresult.result;
        }
      } else if (foundry.dice.terms.DiceTerm.compareResult(r.result, "<", lowtarget)) {
        r.exploded = true;
        let discard = { ...r };
        discard.discarded = true;
        discard.active = false;
        newresults.push(discard);
        let newrollindex = this.results.length;
        await this.roll();
        thisdepth--;
        let newresult = this.results[newrollindex];
        newresult.discarded = true;
        newresult.active = false;
        newresult.result = -newresult.result;
        newresults.push(newresult);
        r.result += newresult.result;
        while (foundry.dice.terms.DiceTerm.compareResult(-newresult.result, ">", hightarget) && thisdepth > 0) {
          newresult.exploded = true;
          newrollindex++;
          await this.roll();
          thisdepth--;
          newresult = this.results[newrollindex];
          newresult.discarded = true;
          newresult.active = false;
          newresult.result = -newresult.result;
          newresults.push(newresult);
          r.result += newresult.result;
        }
      }
    }
    this.results = newresults;
  };

  // Which actor in this browser most recently made a roll
  game.settings.register("grpga", "currentActor", {
    name: "SETTINGS.currentActor.name",
    hint: "SETTINGS.currentActor.hint",
    scope: "client",
    config: false,
    default: "null",
    type: String
  });

  // Which Ruleset are we using
  game.settings.register("grpga", "rulesetChoice", {
    name: "SETTINGS.rulesetChoice.name",
    hint: "SETTINGS.rulesetChoice.hint",
    scope: "world",
    config: true,
    default: "d120",
    type: String,
    requiresReload: true,
    choices: {
      "3d6": "A 3D6 Ruleset like GURPS", // "generic": "A flexible and Generic Ruleset",
      "d120": "A Palladium Games Ruleset (or hijack)",
      "heroic": "A HEROIC! Ruleset",
      "d6": "A Flexible D6 Ruleset",
      "d100": "An Open-Ended D100 Ruleset",
      "mnm": "Mutants & Masterminds Ruleset"
    }
  });
  const rschoice = game.settings.get("grpga", "rulesetChoice");

  // Which Status Effects are we using
  game.settings.register("grpga", "statusEffectChoice", {
    name: "SETTINGS.statusEffectChoice.name",
    hint: "SETTINGS.statusEffectChoice.hint",
    scope: "world",
    config: true,
    default: "ruleset",
    type: String,
    requiresReload: true,
    choices: {
      "ruleset": "Those prepared for the Ruleset",
      "default": "The Foundry default Effects",
      "user-defined": "Those contained in a 'Status Effects' compendium"
    }
  });

  // Are we hijacking Palladium for another game system
  game.settings.register("grpga", "palladiumHijack", {
    name: "SETTINGS.palladiumHijack.name",
    hint: "SETTINGS.palladiumHijack.hint",
    scope: "world",
    config: true,
    default: "UsePal",
    type: String,
    requiresReload: true,
    choices: {
      "UsePal": "SETTINGS.UsePal",
      "UseD6Pool": "SETTINGS.UseD6Pool",
      "UsePnP": "SETTINGS.UsePnP",
      "Use2D6": "SETTINGS.Use2D6",
      "Use3D6": "SETTINGS.Use3D6",
      "UseD10": "SETTINGS.UseD10",
      "Use2D10": "SETTINGS.Use2D10",
      "UseNovus": "SETTINGS.UseNovus",
      "UseD20": "SETTINGS.UseD20",
      "UseAlternity": "SETTINGS.UseAlternity",
      "UsePercent": "SETTINGS.UsePercent"
    }
  });
  CONFIG.system.hijack = game.settings.get("grpga", "palladiumHijack");

  // Register whether modifying a hijack with Sine Nomine conventions
  game.settings.register("grpga", "useSineNomine", {
    name: "SETTINGS.UseSineNomine",
    hint: "SETTINGS.UseSineNomineHint",
    scope: "world",
    config: true,
    default: false,
    type: Boolean
  });
  CONFIG.system.useSineNomine = game.settings.get("grpga", "useSineNomine");

  // Register whether modifying a hijack with Marvel 616 dice conventions
  game.settings.register("grpga", "use616dice", {
    name: "SETTINGS.Use616dice",
    hint: "SETTINGS.Use616diceHint",
    scope: "world",
    config: true,
    default: false,
    type: Boolean
  });
  CONFIG.system.use616dice = game.settings.get("grpga", "use616dice");

  if (rschoice != "d120") {
    CONFIG.system.hijack = "UsePal";
    CONFIG.system.useSineNomine = false;
    CONFIG.system.use616dice = false;
  }

  // Do you want to use a RollTable to define the stat bonuses?
  game.settings.register("grpga", "statBonusTableName", {
    name: "SETTINGS.statBonusTableName.name",
    hint: "SETTINGS.statBonusTableName.hint",
    scope: "world",
    config: true,
    default: "",
    requiresReload: true,
    type: String,
    onChange: rule => _setStatBonusTable(rule)
  });
  // process this on init even if there is no change
  _setStatBonusTable(game.settings.get("grpga", "statBonusTableName"));

  function _setStatBonusTable(rule) {
    CONFIG.system.statBonusTableName = rule.trim();
  }

  // Do you want to use a RollTable to define the step bonuses?
  game.settings.register("grpga", "stepTableName", {
    name: "SETTINGS.stepTableName.name",
    hint: "SETTINGS.stepTableName.hint",
    scope: "world",
    config: true,
    default: "",
    requiresReload: true,
    type: String,
    onChange: rule => _setStepTable(rule)
  });
  // process this on init even if there is no change
  _setStepTable(game.settings.get("grpga", "stepTableName"));

  function _setStepTable(rule) {
    CONFIG.system.stepTableName = rule.trim();
  }
  
  // Register Macro Dialog Delay
  game.settings.register("grpga", "macroDialogDelay", {
    name: "SETTINGS.macroDialogDelay.name",
    hint: "SETTINGS.macroDialogDelay.hint",
    scope: "client",
    config: true,
    default: 500,
    requiresReload: false,
    type: Number
  });

  // Register Combat Tracker in use
  game.settings.register("grpga", "combatTrackerInUse", {
    name: "SETTINGS.CombatTrackerInUse",
    hint: "SETTINGS.CombatTrackerInUseHint",
    scope: "world",
    config: true,
    default: "default",
    requiresReload: true,
    type: String,
    choices: {
      "default": "Foundry Default",
      "pal": "Palladium/ShadowRun4"
    }
  });
  CONFIG.Combat.tracker = game.settings.get("grpga", "combatTrackerInUse");

  game.settings.register("grpga", "systemMigrationVersion", {
    name: "System Migration Version",
    scope: "world",
    config: true,
    type: Number,
    default: 3.18
  });

  // Register initiative rule
  game.settings.register("grpga", "initiativeRule", {
    name: "SETTINGS.initiativeRule.name",
    hint: "SETTINGS.initiativeRule.hint",
    scope: "world",
    config: true,
    default: "rolled",
    requiresReload: true,
    type: String,
    choices: {
      "default": "SETTINGS.initiativeRule.InitDefault",
      "house": "SETTINGS.initiativeRule.InitHouse",
      "house2": "SETTINGS.initiativeRule.InitHouse2",
      "dnd": "SETTINGS.initiativeRule.D20",
      "d120": "SETTINGS.initiativeRule.D120",
      "d100": "SETTINGS.initiativeRule.D100",
      "rolled": "SETTINGS.initiativeRule.Rolled",
      "unrolled": "SETTINGS.initiativeRule.Unrolled",
      "oats": "SETTINGS.initiativeRule.OaTS",
      "percent": "SETTINGS.initiativeRule.percent"
    },
    onChange: rule => _setInitiativeRule(rule)
  });
  // process this on init even if there is no change
  _setInitiativeRule(game.settings.get("grpga", "initiativeRule"));

  function _setInitiativeRule(initMethod) {
    let formula;
    switch (initMethod) {
      case "default": formula = "(1d100 - 1) / 1000000 + @dynamic.dx.system.value / 10000 + @bs.value"; break;
      case "house": formula = "2d100 / 1000 + @dynamic.dx.system.value / 1000 + @bs.value"; break;
      case "house2": formula = "(1d750 / 1000) - 0.251 + @bs.value"; break;
      case "dnd": formula = "1d20 + @dynamic.initiative.system.moddedvalue"; break;
      case "d120": formula = "1d20 + @dynamic.initiative.system.moddedvalue + (1d100 - 1) / 100"; break;
      case "oats": formula = "3d6 + @dynamic.initiative.system.moddedvalue + (1d100 - 1) / 100"; break;
      case "d100": formula = "2d10 + @dynamic.initiative.system.moddedvalue"; break;
      case "rolled": formula = "@bs.value"; break;
      case "unrolled": formula = "@dynamic.initiative.system.moddedvalue"; break;
      case "percent": formula = "1d10 + @dynamic.initiative.system.moddedvalue"; break;
    }

    let decimals = (initMethod == "default") ? 6 : 3;
    CONFIG.Combat.initiative = {
      formula: formula,
      decimals: decimals
    }
  }

  if (CONFIG.system[rschoice]) {
    // Register Primary Attributes
    game.settings.register("grpga", "primaryAttributes", {
      name: "SETTINGS.primaryAttributes.name",
      hint: "SETTINGS.primaryAttributes.hint",
      scope: "world",
      config: true,
      restricted: true,
      default: CONFIG.system[rschoice].primaryAttributes.toString(),
      requiresReload: true,
      type: String
    });
    CONFIG.system[rschoice].primaryAttributes = game.settings.get("grpga", "primaryAttributes").split(",").map(word => word.trim());

    // Register Header Variables
    game.settings.register("grpga", "headerVariables", {
      name: "SETTINGS.headerVariables.name",
      hint: "SETTINGS.headerVariables.hint",
      scope: "world",
      config: true,
      restricted: true,
      default: CONFIG.system[rschoice].headerVariables.toString(),
      requiresReload: true,
      type: String
    });
    CONFIG.system[rschoice].headerVariables = game.settings.get("grpga", "headerVariables").split(",").map(word => word.trim());

    // Register Skill Variables
    game.settings.register("grpga", "skillVariables", {
      name: "SETTINGS.skillVariables.name",
      hint: "SETTINGS.skillVariables.hint",
      scope: "world",
      config: true,
      restricted: true,
      default: CONFIG.system[rschoice].skillVariables.toString(),
      requiresReload: true,
      type: String
    });
    CONFIG.system[rschoice].skillVariables = game.settings.get("grpga", "skillVariables").split(",").map(word => word.trim());

    // Register Spell Variables
    game.settings.register("grpga", "spellVariables", {
      name: "SETTINGS.spellVariables.name",
      hint: "SETTINGS.spellVariables.hint",
      scope: "world",
      config: true,
      restricted: true,
      default: CONFIG.system[rschoice].spellVariables.toString(),
      requiresReload: true,
      type: String
    });
    CONFIG.system[rschoice].spellVariables = game.settings.get("grpga", "spellVariables").split(",").map(word => word.trim());

    // Register Defence Variables
    game.settings.register("grpga", "defenceVariables", {
      name: "SETTINGS.defenceVariables.name",
      hint: "SETTINGS.defenceVariables.hint",
      scope: "world",
      config: true,
      restricted: true,
      default: CONFIG.system[rschoice].defenceVariables.toString(),
      requiresReload: true,
      type: String
    });
    CONFIG.system[rschoice].defenceVariables = game.settings.get("grpga", "defenceVariables").split(",").map(word => word.trim());

    // Register Attack Variables
    game.settings.register("grpga", "attackVariables", {
      name: "SETTINGS.attackVariables.name",
      hint: "SETTINGS.attackVariables.hint",
      scope: "world",
      config: true,
      restricted: true,
      default: CONFIG.system[rschoice].attackVariables.toString(),
      requiresReload: true,
      type: String
    });
    CONFIG.system[rschoice].attackVariables = game.settings.get("grpga", "attackVariables").split(",").map(word => word.trim());
  };

  // Register Automatically making critical rolls
  game.settings.register("grpga", "autoCriticalRolls", {
    name: "SETTINGS.autoCriticalRolls.name",
    hint: "SETTINGS.autoCriticalRolls.hint",
    scope: "world",
    config: true,
    default: true,
    type: Boolean
  });

  // Register Show Test Data
  game.settings.register("grpga", "showTestData", {
    name: "SETTINGS.showTestData.name",
    hint: "SETTINGS.showTestData.hint",
    scope: "world",
    config: true,
    default: false,
    requiresReload: false,
    type: Boolean,
    onChange: rule => _setTestMode(rule)
  });
  // process this on init even if there is no change
  _setTestMode(game.settings.get("grpga", "showTestData"));

  function _setTestMode(rule) {
    CONFIG.system.testMode = rule;
  }

  // Register Show Hooks Data
  game.settings.register("grpga", "showHooks", {
    name: "SETTINGS.showHooks.name",
    hint: "SETTINGS.showHooks.hint",
    scope: "world",
    config: true,
    default: false,
    requiresReload: false,
    type: Boolean,
    onChange: rule => _setShowHooks(rule)
  });
  // process this on init even if there is no change
  _setTestMode(game.settings.get("grpga", "showHooks"));

  function _setShowHooks(rule) {
    CONFIG.debug.hooks = rule;
  }

  game.settings.register("grpga", "narrateJustifyLeft", {
    name: "SETTINGS.narrateJustifyLeft.name",
    hint: "SETTINGS.narrateJustifyLeft.hint",
    scope: "client",
    config: true,
    default: false,
    requiresReload: true,
    type: Boolean
  });
  CONFIG.system.narrateJustifyLeft = game.settings.get("grpga", "narrateJustifyLeft");

  // Register whether using FourAmigos House Rules
  game.settings.register("grpga", "useFAHR", {
    name: "SETTINGS.useFAHR.name",
    hint: "SETTINGS.useFAHR.hint",
    scope: "world",
    config: true,
    default: false,
    requiresReload: true,
    type: Boolean
  });

  /*
  game.settings.register("grpga", "effectSize", {
    name: "SETTINGS.TokenEffectSize",
    hint: "SETTINGS.TokenEffectSizeHint",
    default: CONFIG.tokenEffects.effectSizeChoices.small,
    scope: "client",
    type: String,
    choices: CONFIG.tokenEffects.effectSizeChoices,
    config: true,
    onChange: s => {
      TokenEffects.patchCore();
      canvas.draw();
    }
  });
  TokenEffects.patchCore();
*/
  // ================ end of system settings

  // ================ beginning of system setup
  if (CONFIG.Combat.tracker == "pal") {
    CONFIG.Combat.documentClass = ActionPointCombat;
    CONFIG.ui.combat = ActionPointCombatTracker;
    CONFIG.Combatant.documentClass = ActionPointCombatant;
    CONFIG.Combat.sheetClass = ActionPointCombatantConfig;
    CONFIG.time.roundTime = 10;
  } else {
    // use the Foundry Default settings
    CONFIG.Combat.documentClass = GRPGACombat;
  }

  // Change the thickness of the border around Objects. Default = 4
  CONFIG.Canvas.objectBorderThickness = 8;
  CONFIG.system.ruleset = rschoice;

  switch (rschoice) {
    case "3d6": {
      CONFIG.system.chartype = "Character3D6";
      CONFIG.system.itemtypes = [
        "Modifier",
        "Variable",
        "Pool",
        "Defence",
        "Equipment",
        "Hit-Location",
        "Melee-Attack",
        "Primary-Attribute",
        "Ranged-Attack",
        "Rollable",
        "Container",
        "Trait"
      ];
      break;
    }
    case "d100": {
      CONFIG.system.chartype = "CharacterD100";
      CONFIG.system.itemtypes = [
        "Modifier",
        "Variable",
        "Pool",
        "Defence",
        "Equipment",
        "Hit-Location",
        "Melee-Attack",
        "Primary-Attribute",
        "Ranged-Attack",
        "Rollable",
        "Container",
        "Trait",
        "Rank-Progression",
        "Wound"
      ];
      break;
    }
    case "mnm": {
      CONFIG.system.chartype = "CharacterMnM";
      CONFIG.system.itemtypes = [
        "Modifier",
        "Variable",
        "Pool",
        "Defence",
        "Equipment",
        "Hit-Location",
        "Melee-Attack",
        "Primary-Attribute",
        "Ranged-Attack",
        "Rollable",
        "Container",
        "Trait",
        "Power",
        "Advantage"
      ];
      break;
    }
    case "heroic": {
      CONFIG.system.chartype = "CharacterHeroic";
      CONFIG.system.itemtypes = [
        "Modifier",
        "Variable",
        "Pool",
        "Defence",
        "Primary-Attribute",
        "Rollable",
        "Container",
        "Trait",
        "Power"
      ];
      break;
    }
    case "d6": {
      CONFIG.system.chartype = "CharacterD6";
      CONFIG.system.itemtypes = [
        "Modifier",
        "Variable",
        "Pool",
        "Defence",
        "Melee-Attack",
        "Primary-Attribute",
        "Ranged-Attack",
        "Container",
        "Trait",
        "Rollable"
      ];
      break;
    }
    case "generic": {
      CONFIG.system.chartype = "CharacterGeneric";
      CONFIG.system.itemtypes = [
        "Modifier",
        "Variable",
        "Pool",
        "Defence",
        "Equipment",
        "Hit-Location",
        "Melee-Attack",
        "Primary-Attribute",
        "Ranged-Attack",
        "Rollable",
        "Container",
        "Trait",
        "Rank-Progression"
      ];
      break;
    }
    default: {
      CONFIG.system.chartype = "CharacterD120";
      CONFIG.system.itemtypes = [
        "Modifier",
        "Variable",
        "Pool",
        "Defence",
        "Equipment",
        "Hit-Location",
        "Melee-Attack",
        "Primary-Attribute",
        "Ranged-Attack",
        "Rollable",
        "Container",
        "Trait",
        "Rank-Progression"
      ];
      break;
    }
  }

  // Register sheet application classes
  Actors.unregisterSheet("core", ActorSheet);
  Items.unregisterSheet("core", ItemSheet);

  Actors.registerSheet("grpga", ActorGenericSheet, {
    types: ["CharacterGeneric"],
    makeDefault: true,
    label: "Dynamic Generic Character"
  });
  Actors.registerSheet("grpga", Actor3D6Sheet, {
    types: ["Character3D6"],
    makeDefault: true,
    label: "Dynamic 3D6 Character"
  });
  Actors.registerSheet("grpga", Actor3D6FAHRSheet, {
    types: ["Character3D6"],
    makeDefault: false,
    label: "Four Amigos Style 3D6 Character"
  });

  Actors.registerSheet("grpga", ActorD100Sheet, {
    types: ["CharacterD100"],
    makeDefault: true,
    label: "Dynamic D100 Character"
  });

  Actors.registerSheet("grpga", ActorMnMSheet, {
    types: ["CharacterMnM"],
    makeDefault: true,
    label: "Mutants & Masterminds Character"
  });

  Actors.registerSheet("grpga", ActorD6Sheet, {
    types: ["CharacterD6"],
    makeDefault: true,
    label: "D6 Character"
  });

  Actors.registerSheet("grpga", ActorPalladiumSheet, {
    types: ["CharacterD120"],
    makeDefault: true,
    label: "Palladium Character"
  });

  Actors.registerSheet("grpga", ActorD120Sheet, {
    types: ["CharacterD120"],
    makeDefault: false,
    label: "D120 Character"
  });

  Actors.registerSheet("grpga", ActorHeroicSheet, {
    types: ["CharacterHeroic"],
    makeDefault: true,
    label: "HEROIC! Character"
  });

  Items.registerSheet("grpga", BaseItemSheet, {
    types: ["Primary-Attribute", "Melee-Attack", "Ranged-Attack", "Rank-Progression", "Rollable", "Power", "Defence", "Equipment", "Hit-Location", "Advantage", "Wound"],
    makeDefault: true,
    label: "Items"
  });
  Items.registerSheet("grpga", PoolItemSheet, {
    types: ["Pool"],
    makeDefault: true,
    label: "Pool Item"
  });
  Items.registerSheet("grpga", ModifierItemSheet, {
    types: ["Modifier"],
    makeDefault: true,
    label: "Modifier Item"
  });
  Items.registerSheet("grpga", VariableItemSheet, {
    types: ["Variable"],
    makeDefault: true,
    label: "Variable Item"
  });
  Items.registerSheet("grpga", ContainerItemSheet, {
    types: ["Container"],
    makeDefault: true,
    label: "Container Item"
  });
  Items.registerSheet("grpga", TraitItemSheet, {
    types: ["Trait"],
    makeDefault: true,
    label: "Trait Item"
  });

  preloadHandlebarsTemplates();

  // If you need to add Handlebars helpers, here are a few useful examples:
  Handlebars.registerHelper('concat', function () {
    var outStr = '';
    for (var arg in arguments) {
      if (typeof arguments[arg] != 'object') {
        outStr += arguments[arg];
      }
    }
    return outStr;
  });

  Handlebars.registerHelper('toLowerCase', function (str) {
    return str.toLowerCase();
  });

  Handlebars.registerHelper('slugify', function (str) {
    return system.slugify(str);
  });

  Handlebars.registerHelper('toSentenceCase', function (str) {
    return str.replace(
      /\w\S*/g,
      function (txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
      }
    );
  });

  Handlebars.registerHelper('isnum', function (num) {
    return Number.isInteger(num);
  });

  Handlebars.registerHelper('contains', function (str, text) {
    return str.includes(text);
  });

  Handlebars.registerHelper('debug', function (text, content) {
    return console.debug(text, content);
  });

  Handlebars.registerHelper('isModulus', function (value, div, rem) {
    return value % div == rem;
  });

  Handlebars.registerHelper('times', function (n, content) {
    let result = "";
    for (let i = 0; i < n; ++i) {
      result += content.fn(i);
    }
    return result;
  });

  /**
   * A helper to create a set of radio checkbox input elements in a named set.
   * The provided keys are the possible radio values while the provided values are human readable labels.
   *
   * @param {string} name         The radio checkbox field name
   * @param {object} choices      A mapping of radio checkbox values to human readable labels
   * @param {string} options.checked    Which key is currently checked?
   * @param {boolean} options.localize  Pass each label through string localization?
   * @return {Handlebars.SafeString}
   *
   * @example <caption>The provided input data</caption>
   * let groupName = "importantChoice";
   * let choices = {a: "Choice A", b: "Choice B"};
   * let chosen = "a";
   *
   * @example <caption>The template HTML structure</caption>
   * <div class="form-group">
   *   <label>Radio Group Label</label>
   *   <div class="form-fields">
   *     {{radioBoxes groupName choices checked=chosen localize=true}}
   *   </div>
   * </div>
   */
  Handlebars.registerHelper('radioLabels', function (name, choices, options) {
    const checked = options.hash['checked'] || null;
    const localize = options.hash['localize'] || false;
    let html = "";
    for (let [key, label] of Object.entries(choices)) {
      if (localize) label = game.i18n.localize(label);
      const isChecked = checked === key;
      html += `<input type="radio" id="${label}" name="${name}" value="${key}" ${isChecked ? "checked" : ""}><label for="${label}">${label}</label>`;
    }
    return new Handlebars.SafeString(html);
  });
});

Hooks.once('ready', async function () {
  system.modeSpecificText = {
    attr: game.i18n.localize(`local.item.Primary-Attribute.${CONFIG.system.ruleset}.attr`),
    formula: game.i18n.localize(`local.item.Melee-Attack.${CONFIG.system.ruleset}.formula`),
    damage: game.i18n.localize(`local.item.Melee-Attack.${CONFIG.system.ruleset}.damage`),
    damageType: game.i18n.localize(`local.item.Melee-Attack.${CONFIG.system.ruleset}.damageType`),
    armourDiv: game.i18n.localize(`local.item.Melee-Attack.${CONFIG.system.ruleset}.armourDiv`),
    minST: game.i18n.localize(`local.item.Melee-Attack.${CONFIG.system.ruleset}.minST`),
    accuracy: game.i18n.localize(`local.item.Ranged-Attack.${CONFIG.system.ruleset}.accuracy`),
    targets: game.i18n.localize(`local.item.Modifier.${CONFIG.system.ruleset}.targets`)
  }
  system.selectOptions = {
    severity: [
      { value: "na", label: "local.item.Hit-Location.d100.na" },
      { value: "minor", label: "local.item.Hit-Location.d100.minor" },
      { value: "serious", label: "local.item.Hit-Location.d100.serious" },
      { value: "crippling", label: "local.item.Hit-Location.d100.crippling" },
    ],
    treatment: [
      { value: "none", label: "local.item.Hit-Location.d100.none" },
      { value: "critical", label: "local.item.Hit-Location.d100.critical" },
      { value: "failure", label: "local.item.Hit-Location.d100.failure" },
      { value: "partial", label: "local.item.Hit-Location.d100.partial" },
      { value: "success", label: "local.item.Hit-Location.d100.success" },
      { value: "outstanding", label: "local.item.Hit-Location.d100.outstanding" },
    ],
    rolltype: [
      { value: "std", label: "local.item.Melee-Attack.d100.std" },
      { value: "oe", label: "local.item.Melee-Attack.d100.oe" },
      { value: "oeh", label: "local.item.Melee-Attack.d100.oeh" },
      { value: "oel", label: "local.item.Melee-Attack.d100.oel" },
    ],
    modifier_heroic_category: [
      { value: "defence", label: `local.item.Modifier.heroic.defence` },
      { value: "spell", label: `local.item.Rollable.heroic.rms` },
      { value: "primary", label: "local.item.Modifier.primary" }
    ],
    modifier_d6_category: [
      { value: "attack", label: "local.item.Modifier.attack" },
      { value: "defence", label: `local.item.Modifier.d6.defence` },
      { value: "skill", label: `local.item.Rollable.d6.skill` },
      { value: "damage", label: "local.item.Modifier.damage" },
      { value: "primary", label: "local.item.Modifier.primary" }
    ],
    modifier_3d6_category: [
      { value: "attack", label: "local.item.Modifier.attack" },
      { value: "defence", label: `local.item.Modifier.3d6.defence` },
      { value: "skill", label: `local.item.Rollable.3d6.skill` },
      { value: "spell", label: `local.item.Rollable.3d6.spell` },
      { value: "check", label: `local.item.Rollable.3d6.check` },
      { value: "reaction", label: `local.item.Modifier.3d6.reaction` },
      { value: "damage", label: "local.item.Modifier.damage" },
      { value: "primary", label: "local.item.Modifier.primary" }
    ],
    modifier_d100_category: [
      { value: "attack", label: "local.item.Modifier.attack" },
      { value: "defence", label: `local.item.Modifier.d100.defence` },
      { value: "skill", label: `local.item.Rollable.d100.skill` },
      { value: "spell", label: `local.item.Rollable.d100.spell` },
      { value: "check", label: `local.item.Rollable.d100.check` },
      { value: "primary", label: "local.item.Modifier.primary" }
    ],
    modifier_category: [
      { value: "attack", label: "local.item.Modifier.attack" },
      { value: "defence", label: game.i18n.localize(`local.item.Modifier.${CONFIG.system.ruleset}.defence`) },
      { value: "skill", label: game.i18n.localize(`local.item.Rollable.${CONFIG.system.ruleset}.skill`) },
      { value: "spell", label: game.i18n.localize(`local.item.Rollable.${CONFIG.system.ruleset}.spell`) },
      { value: "check", label: game.i18n.localize(`local.item.Rollable.${CONFIG.system.ruleset}.check`) },
      { value: "damage", label: "local.item.Modifier.damage" },
      { value: "primary", label: "local.item.Modifier.primary" }
    ],
    trait_category: [
      { value: "advantage", label: game.i18n.localize(`local.item.Trait.${CONFIG.system.ruleset}.advantage`) },
      { value: "disadvantage", label: game.i18n.localize(`local.item.Trait.${CONFIG.system.ruleset}.disadvantage`) },
      { value: "perk", label: game.i18n.localize(`local.item.Trait.${CONFIG.system.ruleset}.perk`) },
      { value: "quirk", label: game.i18n.localize(`local.item.Trait.${CONFIG.system.ruleset}.quirk`) }
    ],
    rollable_category: [
      { value: "check", label: game.i18n.localize(`local.item.Rollable.${CONFIG.system.ruleset}.check`) },
      { value: "skill", label: game.i18n.localize(`local.item.Rollable.${CONFIG.system.ruleset}.skill`) },
      { value: "spell", label: game.i18n.localize(`local.item.Rollable.${CONFIG.system.ruleset}.spell`) },
      { value: "technique", label: game.i18n.localize(`local.item.Rollable.${CONFIG.system.ruleset}.technique`) },
      { value: "rms", label: game.i18n.localize(`local.item.Rollable.${CONFIG.system.ruleset}.rms`) }
    ],
    rollable_d6_category: [
      { value: "skill", label: `local.item.Rollable.d6.skill` },
      { value: "technique", label: `local.item.Rollable.d6.technique` },
      { value: "rms", label: `local.item.Rollable.d6.rms` }
    ],
    rollable_heroic_category: [
      { value: "rms", label: `local.item.Rollable.heroic.rms` }
    ],
    defence_category: [
      { value: "dodge", label: game.i18n.localize(`local.item.Defence.${CONFIG.system.ruleset}.dodge`) },
      { value: "parry", label: game.i18n.localize(`local.item.Defence.${CONFIG.system.ruleset}.parry`) },
      { value: "block", label: game.i18n.localize(`local.item.Defence.${CONFIG.system.ruleset}.block`) }
    ],
    defence_d6_category: [
      { value: "dodge", label: `local.item.Defence.d6.dodge` },
      { value: "block", label: `local.item.Defence.d6.block` }
    ],
    postures: [
      { value: "standing", label: "local.postures.standing" },
      { value: "crouching", label: "local.postures.crouching" },
      { value: "kneeling", label: "local.postures.kneeling" },
      { value: "crawling", label: "local.postures.crawling" },
      { value: "sitting", label: "local.postures.sitting" },
      { value: "pronef", label: "local.postures.pronef" },
      { value: "proneb", label: "local.postures.proneb" }
    ]
  };

  switch (game.settings.get("grpga", "statusEffectChoice")) {
    case "default":
      break;
    case "ruleset":
      CONFIG.statusEffects = CONFIG[`statusEffects${CONFIG.system.ruleset}`];
      break;
    case "user-defined": {
      const temp = await game.packs.get(`world.status-effects`)?.getDocuments() || [];
      console.log("Status Effects", temp);
      const effects = [];
      temp.forEach(function (effect) {
        effects.push({
          img: effect.img,
          id: system.slugify(effect.name),
          name: effect.name,
          itemId: effect._id
        });
      });
      CONFIG.statusEffects = effects;
      break;
    }
  }

});


Hooks.on('getSceneControlButtons', (controls) => {
  console.debug("Get Scene Control Buttons");
  // Assign Status Effects
  controls[0].tools.push({
    name: "assign",
    title: "HUD.AssignStatusEffects",
    icon: "fa-solid fa-user-gear",
    onClick: async () => {
      const content = await renderTemplate("systems/grpga/templates/rollmacros/status-effect-toggle.hbs", {
        statusEffects: CONFIG.statusEffects
      });

      new MyDialog({
        title: "Assign Status Effects",
        buttons: [],
        content: content,
      }, {
        width: 200,
        height: "fit-content",
        classes: ["mydialog"],
        top: 70,
        left: 110,
      }).render(true);
    },
    button: true
  });
});

/**
 * ################### Active Effect Hooks ###################
 */
Hooks.on('createActiveEffect', async (effect, options, user) => {
  if (user != game.user.id) return;
  const userEffect = game.packs.get('world.status-effects').index.getName(effect.name);
  if (userEffect) {
    const effectItem = await fromUuid(userEffect.uuid);
    await effect.parent.createEmbeddedDocuments("Item", [effectItem]);
  }
  console.debug("Create", [effect]);
});

Hooks.on('deleteActiveEffect', async (effect, options, user) => {
  if (user != game.user.id) return;
  const existing = effect.parent.items.filter(item => item.name == effect.name && item.type == "Modifier");
  const olditems = [];
  while (existing[0]) {
    olditems.push(existing.pop().id)
  }
  await effect.parent.deleteEmbeddedDocuments("Item", olditems);
  console.debug("Delete", [effect]);
});

/**
 * ################### Actor Hooks ###################
 */

/**
* Organise some data before rendering the sheet
*/
Hooks.on("getBaseActorSheetHeaderButtons", (sheet, buttonarray) => {
  if (CONFIG.system.testMode) { console.debug("getBaseActorSheetHeaderButtons\n", [sheet, buttonarray]); }
  return;
  // This happens before the call to render the sheet
});

/**
* Organise some data before rendering the sheet
*/
Hooks.on("renderBaseActorSheet", (sheet, html, data) => {
  const actor = sheet.actor;
  if (CONFIG.system.testMode) { console.debug("renderBaseActorSheet\n", [sheet, html, data, actor.system]); }
  return;
});

/**
* Organise some data before closing the sheet
*
* Reset temporary modifiers and variables
*/
Hooks.on("closeActorSheet", (sheet, html) => {
  const actor = sheet.actor;
  if (CONFIG.system.testMode) { console.debug("closeActorSheet\n", [sheet, html, actor.system]); }
  return;
});

/**
 * Specific Tailoring of actor types on creation
 * 
 * Preparing to fake actor subclasses and need to make all actor templates the same
 */
Hooks.on('preCreateActor', (document, data, options, userId) => {
  if (CONFIG.system.testMode) console.debug("preCreateActor:\n", [document, data, options, userId]);
  // if the actor has an image, it already exists. No reason to be doing anything to it here yet.
  if (data.img) return;

  const newdata = {};

  // set the basic features according to system requirements
  switch (data.type) {
    case "CharacterMnM": {
      newdata.bm = {
        step: 30
      }
      break;
    }
    case "Character3D6": {
      newdata.bm = {
        step: 1,
        quarter: 1,
        half: 2,
        move: 5,
        sprint: 6
      }
      break;
    }
    case "CharacterD120": {
      newdata.bm = {
        step: 30
      }
      break;
    }
    case "CharacterD100": {
      newdata.bm = {
        step: 50
      }
      break;
    }
  }
  data.system = newdata;

  document.updateSource(data);
});


/**
 * Specific Tailoring of actor types on creation
 * 
 * Preparing to fake actor subclasses and need to make all actor templates the same
 */
Hooks.on('preUpdateActor', (document, data, options, userId) => {
  if (CONFIG.system.testMode) console.debug("preUpdateActor:\n", [document, data, options, userId]);

  // if diff is false it is probably a data import otherwise return
  if (options.diff) return;

  try {
    console.debug(ImagePopout.getImageSize(data.img));
    return;
  } catch (err) {
    console.error(err);
  }

  // if you get here, there is no image or they are in a place we cannot be sure to reach
  data.img = "icons/svg/mystery-man-black.svg";
  data.token.img = "icons/svg/mystery-man-black.svg";

  document.updateSource(data);
});

/**
 * ################### Canvas Hooks ###################
 */

Hooks.on('dropCanvasData', (canvas, data) => {
  // is there a token under the drop point?
  for (let token of canvas.tokens.ownedTokens) {
    if (token.x > data.x) continue;
    if (token.y > data.y) continue;
    if (token.x + token.w < data.x) continue;
    if (token.y + token.h < data.y) continue;

    return token.actor.sheet._onTokenDrop(data);
  }
});

/**
 * ################### Chat Hooks ###################
 */

Hooks.on('preCreateChatMessage', (message, options, userid) => {
  // used to be for hiding initiative rolls by returning false.
});

/**
 * add listeners to chat messages in the log
 */

Hooks.on("renderChatLog", (log, html, data) => {

  html.on('click', '.open-journal', ev => {
    ev.preventDefault();

    const journal = ev.currentTarget.dataset.table;
    try {
      Journal._showEntry(game.journal.getName(journal).uuid, "text", true);
    } catch (err) {
      if (journal) ui.notifications.error(`There is no matching Journal Entry for ${journal}. Perhaps you have yet to import it from the compendium?`);
    }
  });

});

Hooks.on("renderChatMessage", async (app, html, msg) => {
  const actor = game.actors.get(msg.message.speaker.actor) || null;

  // the Narrative Tools module with left-justification instead of centred
  if (CONFIG.system.narrateJustifyLeft) {
    html[0].classList.add('left');
  };

  // process the rollable item
  html.on('click', '.rollable', ev => {
    actor?.sheet._onRoll(ev);
  });

  // process the rollmacro item
  html.on('click', '.rollmacro', ev => {
    actor?.sheet._rollmacro(ev);
  });

  html.find(".critresult").each(function () {
    let exp = $(this)[0]
    exp.setAttribute("draggable", true)
    exp.addEventListener('dragstart', ev => {
      let dataTransfer = {
        type: $(exp).attr("data-type"),
        message: $(exp).attr("data-message"),
        woundtitle: $(exp).attr("data-woundtitle"),
        condition: $(exp).attr("data-condition"),
        hits: $(exp).attr("data-hits"),
        bleed: $(exp).attr("data-bleed"),
        action: $(exp).attr("data-action"),
        effect: $(exp).attr("data-effect")
      }
      ev.dataTransfer.setData("text/plain", JSON.stringify(dataTransfer));
    })
  });

  html.find(".damageresult").each(function () {
    let exp = $(this)[0]
    exp.setAttribute("draggable", true)
    exp.addEventListener('dragstart', ev => {
      let dataTransfer = {
        type: $(exp).attr("data-type"),
        hits: $(exp).attr("data-hits")
      }
      ev.dataTransfer.setData("text/plain", JSON.stringify(dataTransfer));
    })
  });

  html.find(".dynamicitem").each(function () {
    let exp = $(this)[0]
    exp.setAttribute("draggable", true)
    exp.addEventListener('dragstart', ev => {
      let dataTransfer = {
        type: $(exp).attr("data-type"),
        id: exp.parentNode.parentNode.dataset.messageId
      }
      ev.dataTransfer.setData("text/plain", JSON.stringify(dataTransfer));
    })
  });

  html.find(".defencevalue").each(function () {
    let exp = $(this)[0]
    exp.setAttribute("draggable", true)
    exp.addEventListener('dragstart', ev => {
      let dataTransfer = {
        type: $(exp).attr("data-type"),
        db: $(exp).attr("data-db")
      }
      ev.dataTransfer.setData("text/plain", JSON.stringify(dataTransfer));
    })
  });

  html.find(".critseverity").each(function () {
    let exp = $(this)[0]
    exp.setAttribute("draggable", true)
    exp.addEventListener('dragstart', ev => {
      let dataTransfer = {
        type: $(exp).attr("data-type"),
        crit: $(exp).attr("data-crit")
      }
      ev.dataTransfer.setData("text/plain", JSON.stringify(dataTransfer));
    })
  });

});

/**
 * ################### Dialog Hooks ###################
 */


/**
* add listeners to dialog boxes
*/
Hooks.on("renderDialog", (dialog, html, data) => {

  if (data.content?.includes("statuseffectsdialog")) {

    // toggle a status effect on selected tokens
    html.on('click', '.status-effect', ev => {
      console.log("Status Effect Toggle", ev);
      const statusId = ev.currentTarget.dataset.statusId;
      if (canvas.tokens.controlled.length == 0) {
        ui.notifications.error("Please select at least one token first");
        return;
      }
      for (const token of canvas.tokens.controlled) {
        token.document.actor.toggleStatusEffect(statusId);
      }
    });
  }

  // isolate these listeners to mydialog windows
  if (data.content?.includes("mydialog")) {
    const actor = dialog.data.options.actor;
    const macroData = dialog.data.options.macroData;
    const delay = game.settings.get("grpga", "macroDialogDelay");

    // Open Journal Entries from a dialog window
    html.on('click', '.open-journal', ev => {
      const journal = ev.currentTarget.dataset.table;
      try {
        Journal._showEntry(game.journal.getName(journal).uuid, "text", true);
      } catch (err) {
        if (journal) ui.notifications.error(`There is no matching Journal Entry for ${journal}. Perhaps you have yet to import it from the compendium?`);
      }
    });

    html.on("change", "select", async ev => {
      if (macroData || ev.currentTarget.name == "") {
        // do nothing. It will be handled by iteminput below
      } else {
        await actor.update({ [ev.currentTarget.name]: ev.currentTarget.value });
        const type = ev.currentTarget.closest(".mydialog").dataset.type;
        actor.actiondialog(type);
        dialog.close();
      }
    });

    // Update the GMod
    html.on('change', '.iteminput', async ev => {
      await actor.sheet._onInputChange(ev);
      await new Promise(r => setTimeout(r, delay));
      if (macroData) {
        macroData.position.left = ev.delegateTarget.offsetLeft;
        macroData.position.top = ev.delegateTarget.offsetTop - 3;
        actor.modifierdialog(ev.currentTarget.dataset.attackId, macroData);
      } else {
        const type = ev.currentTarget.closest(".mydialog").dataset.type;
        actor.actiondialog(type);
      }
      dialog.close();
    });

    // Select the attack to process
    html.on('click', '.selectable', async ev => {
      if (macroData) {
        macroData.position.left = ev.delegateTarget.offsetLeft;
        macroData.position.top = ev.delegateTarget.offsetTop - 3;
        actor.modifierdialog(ev.currentTarget.dataset.itemId, macroData);
      } else {
        if (ev.currentTarget.dataset.type == "refresh" && game.canvas.tokens.controlled?.length == 1) {
          // if the user is the owner of the selected token, open that sheet
          const tokenactor = game.canvas.tokens.controlled[0].actor;
          if (tokenactor?.isOwner) {
            tokenactor.actiondialog();
          }
        } else {
          await actor.update({ "data.bm.step": ev.currentTarget.dataset.direction });
          ev.currentTarget = ev.currentTarget.previousElementSibling;
          await actor.sheet._onInputChange(ev);
          await new Promise(r => setTimeout(r, delay));
          actor.actiondialog();
        }
      }
      dialog.close();
    });

    // process the rollable item
    html.on('click', '.rollable', ev => {
      actor.sheet._onRoll(ev);
      if (macroData && ev.currentTarget.dataset.type != "modlist") dialog.close();
    });

    // process the rollable item
    html.on('click', '.rollmacro', ev => {
      actor.sheet._rollmacro(ev);
    });

    // allow modifiers to be toggled
    html.on('click', '.item-toggle', async ev => {
      await actor.sheet._onToggleItem(ev);
      await new Promise(r => setTimeout(r, delay));
      if (macroData) {
        macroData.position.left = ev.delegateTarget.offsetLeft;
        macroData.position.top = ev.delegateTarget.offsetTop - 3;
        actor.modifierdialog(ev.currentTarget.dataset.attackId, macroData);
      } else {
        const type = ev.currentTarget.closest(".mydialog").dataset.type;
        actor.actiondialog(type);
      }
      dialog.close();
    });
  }
});

/**
 * ################### Item Hooks ###################
 */

Hooks.on('createItem', (document, options, userId) => {
  if (CONFIG.system.testMode) console.debug("createItem:\n", [document, options, userId]);
  const docdata = document.system;

  // items dragged from the sidebar will lose their moddedformula if they have one so calculate it again
  switch (document.type) {
    case "Melee-Attack":
    case "Ranged-Attack":
    case "Defence":
    case "Rollable": {
      if (CONFIG.system.testMode) console.debug("Item Data:\n", docdata);
      if (docdata.formula) { // we will recalculate the value if the formula does not contain an @dependency
        let formula = docdata.formula;
        if (Number.isNumeric(formula)) {
          // the formula is a number
          docdata.value = Number(formula);
        } else if (formula.includes("#") || formula.includes("@")) {
          // the formula has ranks or a reference and will be processed in prepareDerivedData
        } else {
          // the formula should be a valid dice expression so has no value
          docdata.value = 0;
          docdata.moddedvalue = 0;
          docdata.moddedformula = formula;
        }
      }
      break;
    }
    case "Modifier": {
      if (docdata.alwaysOn) docdata.inEffect = true;
      break;
    }
  }
});

Hooks.on('preCreateItem', (document, data, options, userId) => {
  if (CONFIG.system.testMode) console.debug("preCreateItem:\n", [document, data, options, userId]);

  let itemdata = data.system;

  if (document.id) {// This item exists already but may have the wrong chartype
    if (document.actor) {
      // if there is an actor and the chartype matches then exit
      if (document.actor.type == itemdata.chartype) return;
      itemdata.chartype = document.actor.type;
    } else {
      // if the chartype matches the ruleset then exit
      if (CONFIG.system.chartype == itemdata.chartype) return;
      itemdata.chartype = CONFIG.system.chartype;
    }
    // correct the chartype then exit
    document.updateSource(data);
    return;
  } else if (!itemdata) {
    // the item is being created from the item sidebar so give it a chartype
    itemdata = data.system = {};
    itemdata.chartype = CONFIG.system.chartype;
  }

  if (!itemdata.group) { // initialise group field to match item category or type
    itemdata.group = itemdata.category || data.type;
  }

  if (data.name != data.type) {
    // this is an existing item being dropped in a container
    document.updateSource(data);
    return;
  }
  switch (itemdata.chartype) {
    case "CharacterD100": {
      switch (data.type) {
        case "Melee-Attack": {
          data.img = "icons/svg/sword.svg";
          itemdata.armourDiv = 5;
          itemdata.damage = "";
          itemdata.damageType = "";
          itemdata.minST = "oeh";
          itemdata.notes = "Enter the attack and critical table names from the journal entries you have made for them, or leave them blank."
          itemdata.reach = 5;
          itemdata.weight = 100;
          break;
        }
        case "Ranged-Attack": {
          data.img = "icons/svg/target.svg";
          itemdata.armourDiv = 5;
          itemdata.damage = "";
          itemdata.damageType = "";
          itemdata.minST = "oeh";
          itemdata.notes = "Enter the attack and critical table names from the journal entries you have made for them, or leave them blank."
          itemdata.rof = 5;
          itemdata.accuracy = 100;
          itemdata.range = "10";
          break;
        }
        case "Wound": { // Wound
          data.img = "icons/svg/blood.svg";
          data.name = "Wound";
          break;
        }
        case "Defence": { // dodge(std), parry(oeh), block(oe)
          data.img = "icons/svg/mage-shield.svg";
          data.name = "Various";
          break;
        }
        default: {
          data.img = "icons/svg/mystery-man-black.svg";
          break;
        }
      }
      break;
    }
    case "Character3D6": {
      switch (data.type) {
        case "Melee-Attack": {
          data.img = "icons/svg/sword.svg";
          itemdata.formula = "@dx -2";
          itemdata.armourDiv = 1;
          itemdata.damage = "1d6";
          itemdata.damageType = "cut";
          itemdata.minST = "8";
          itemdata.notes = ""
          itemdata.reach = 1;
          itemdata.weight = 0;
          break;
        }
        case "Ranged-Attack": {
          data.img = "icons/svg/target.svg";
          itemdata.formula = "@dx -2";
          itemdata.armourDiv = 1;
          itemdata.damage = "1d6";
          itemdata.damageType = "imp";
          itemdata.minST = "8";
          itemdata.notes = ""
          itemdata.range = "10/15";
          itemdata.rof = 1;
          itemdata.accuracy = 0;
          itemdata.shots = "0";
          itemdata.bulk = 0;
          itemdata.recoil = "0";
          break;
        }
        default: {
          data.img = "icons/svg/mystery-man-black.svg";
          break;
        }
      }
      break;
    }
    default: {
      if (!data.img) {
        data.img = "icons/svg/mystery-man-black.svg";
      }
      break;
    }
  }
  document.updateSource(data);
});

Hooks.on('passCards', (from, to, action) => {
  if (CONFIG.system.testMode) console.debug("passCards:\n", [from, to, options]);

  if (action.action === "play" || action.action === "pass") {
    const chatActions = {
      pass: "CARDS.NotifyPass",
      play: "CARDS.NotifyPlay",
      discard: "CARDS.NotifyDiscard",
      draw: "CARDS.NotifyDraw"
    };
    const chatTo = action.action === "draw" ? from : to;
    const number = action.fromDelete.length;
    const link = chatTo.link;
    const chatAction = chatActions[action.action];
    const messageData = {
      type: CONST.CHAT_MESSAGE_TYPES.OTHER,
      speaker: { user: game.user },
      content: `
      <div class="cards-notification flexrow">
        <p>${game.i18n.format(chatAction, { number: number, link: link })}</p>
      </div>`
    };
    ChatMessage.applyRollMode(messageData, game.settings.get("core", "rollMode"));

    action.fromDelete.forEach(function (c, i) {
      const card = from.cards.get(c);
      card.toMessage(messageData);
      console.log(card);
    });
  }

  return true;
});

/**
 * ################### Journal Hooks ###################
 */

/**
 * add listeners to chat messages in the log
 */
Hooks.on("getJournalSheetHeaderButtons", (log, headerbuttons) => {

  if (log.document.pages.entries().next().value[1].text.content?.includes("spell-choice")) { // this is a spell description journal
    log.position.height = 500;
    log.position.width = 435;
  }
});

/**
* add listeners to rendered journal text pages
*/
Hooks.on("renderJournalTextPageSheet", (log, html, data) => {

  // open a Journal Entry
  html.on('click', '.open-journal', ev => {
    ev.preventDefault();

    const journal = ev.currentTarget.dataset.table;
    Journal._showEntry(game.journal.getName(journal).uuid, "text", true);
  });

  // send a message to the chat log
  html.on('click', '.rollable', ev => {
    ev.preventDefault();
    // data stored in the enclosing element
    let parentset = "";
    // data stored in this element
    let dataset = ev.currentTarget.dataset;
    let message = "";
    let armour = "";
    let crit = "";

    switch (dataset.type) {
      case "attack": {
        parentset = ev.currentTarget.parentElement.dataset;
        switch (dataset.armour) {
          case "na": armour = "No Armor"; break;
          case "la": armour = "Light Armor"; break;
          case "ma": armour = "Medium Armor"; break;
          case "ha": armour = "Heavy Armor"; break;
        }
        switch (dataset.crit) {
          case "Sup": crit = "Superficial"; break;
          case "Lig": crit = "Light +10"; break;
          case "Mod": crit = "Moderate +20"; break;
          case "Gri": crit = "Grievous +30"; break;
          case "Let": crit = "Lethal +50"; break;
        }
        message += `<table class="attack-result"><tr><td>${game.i18n.localize(`local.rolls.table`)}:</td><td>${parentset.table}</td></tr>`;
        message += `<tr><td>${game.i18n.localize(`local.rolls.roll`)}:</td><td>${parentset.roll}</td></tr>`;
        message += `<tr><td>${game.i18n.localize(`local.rolls.against`)}:</td><td>${armour}</td></tr></table>`;
        message += `<p class="damageresult" data-type="damage" data-hits="${dataset.hits}"><b>${dataset.hits}</b> hits</p>`;
        if (crit) {
          message += `<p class="critseverity" data-type="severity" data-crit="${crit}"> and a <b>${crit}</b> critical strike</p>`;
        }
        game.journal.getName(parentset.table).sheet.close();
        break;
      }
      case "fearsaveroll":
      case "reactionrolls":
      case "spellfailure":
      case "spellcasting":
      case "missilefumble":
      case "meleethrownfumble":
      case "magicalresonance": {
        message += `<table class="critical-result"><tr><td>${game.i18n.localize(`local.rolls.table`)}:</td><td>${dataset.table}</td></tr>`;
        message += `<tr><td>${game.i18n.localize(`local.rolls.roll`)}:</td><td>${dataset.roll}</td></tr></table>`;
        message += `<p><b>${dataset.result}</b></p>`;
        break;
      }
      case "critical": {
        // todo: do we have access to the actor name for the speaker?
        // todo: include a link to the crit table we just rolled so it can be re-opened quickly
        message += `<table class="critical-result"><tr><td>${game.i18n.localize(`local.rolls.table`)}:</td><td class="open-journal" data-table="${dataset.table}">${dataset.table}</td></tr>`;
        message += `<tr><td>${game.i18n.localize(`local.rolls.roll`)}:</td><td>${dataset.roll}</td></tr></table>`;
        if (dataset.condition != "null") {
          message += `<p><b>${dataset.result}</b></p>`;
          message += `<p class="critresult" data-type="critical" data-message="${dataset.result}" data-woundtitle="${dataset.woundtitle}" data-hits="${dataset.hits1}" data-bleed="${dataset.bleed1}" data-action="${dataset.action1}" data-effect="${dataset.effect1}">Has ${dataset.condition}</p>`;

          message += `<p class="critresult" data-type="critical" data-message="${dataset.result}" data-woundtitle="${dataset.woundtitle}" data-hits="${dataset.hits2}" data-bleed="${dataset.bleed2}" data-action="${dataset.action2}" data-effect="${dataset.effect2}">Does not</p>`;
        } else { // there is no condition, only a single outcome
          message += `<p class="critresult" data-type="critical" data-message="${dataset.result}" data-woundtitle="${dataset.woundtitle}" data-hits="${dataset.hits1}" data-bleed="${dataset.bleed1}" data-action="${dataset.action1}" data-effect="${dataset.effect1}">${dataset.result}</p>`;
        }
        game.journal.getName(dataset.table).sheet.close();
        break;
      }
      case "spell": {
        message += `<table class="spell-choice"><tr class="open-journal" data-table="${dataset.table}"><td>${game.i18n.localize(`local.rolls.spell.lore`)}:</td><td>${dataset.table}</td></tr>`;
        message += `<tr><td>${game.i18n.localize(`local.rolls.spell.weave`)}:</td><td>${dataset.weave}</td></tr>`;
        message += `<tr class="open-journal" data-table="${dataset.spell}"><td>${game.i18n.localize(`local.rolls.spell.spell`)}:</td><td class="spellname">${dataset.spell}</td></tr>`;
        message += `<tr><td>${game.i18n.localize(`local.rolls.spell.range`)}:</td><td>${dataset.range}</td></tr>`;
        message += `<tr><td>${game.i18n.localize(`local.rolls.spell.aoe`)}:</td><td>${dataset.aoe}</td></tr>`;
        message += `<tr><td>${game.i18n.localize(`local.rolls.spell.duration`)}:</td><td>${dataset.duration}</td></tr>`;
        message += `<tr><td>${game.i18n.localize(`local.rolls.spell.save`)}:</td><td>${dataset.save}</td></tr></table>`;
        // todo: add description and warps for premium content
        game.journal.getName(dataset.spell).sheet.close();
        break;
      }
      default: {
        ui.notifications.info(`Journal rolling for Type:${dataset.type} is not supported yet.`);
        return;
      }
    }
    ChatMessage.create({
      speaker: { actor: game.settings.get("grpga", "currentActor") },
      content: message,
    });
  });
  /*
  let target = token;
  if (!target) return ui.notifications.error(`Please select a single target.`);
  let curtHP = target.actor.getRollData().attributes.hp.value;
  let maxHP = target.actor.getRollData().attributes.hp.max;
  let the_message = `Current Life Total: ${curtHP}/${maxHP}`;
  ChatMessage.create({
    content: the_message,
    whisper: ChatMessage.getWhisperRecipients("GM"),
    speaker: ChatMessage.getSpeaker({ alias: `${token.name} Stats` })
  });
  */
});
